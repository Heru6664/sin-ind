import React from "react";
import App from "./src";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistor, store } from "./src/store/index";
import globalStyles, { colors } from "./src/assets/styles/global";
import { View, Image, StatusBar } from "react-native";
import { H2 } from "native-base";

const Loading = () => (
  <View style={globalStyles.centerContainer}>
    <StatusBar hidden />
    <Image
      source={require("./src/assets/img/SINLogoSmall.png")}
      style={globalStyles.splashScreenLogo}
      resizeMethod="resize"
      resizeMode="contain"
    />
    <View style={{ flexDirection: "row" }}>
      <View
        style={{
          backgroundColor: colors.GREEN_V2,
          marginLeft: 15,
          paddingHorizontal: 10,
          paddingVertical: 3
        }}
      >
        <H2 style={{ color: colors.WHITE, fontWeight: "bold" }}>SIN</H2>
      </View>
      <View style={{ justifyContent: "center", alignItems: "center" }}>
        <H2 style={{ marginLeft: 15, fontWeight: "bold" }}>INDONESIA</H2>
      </View>
    </View>
  </View>
);

export default () => (
  <Provider store={store}>
    <PersistGate loading={<Loading />} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);
