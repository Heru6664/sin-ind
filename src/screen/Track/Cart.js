import {
  Body,
  Button,
  Container,
  Content,
  H2,
  Header,
  Icon,
  Text,
  View,
  Spinner
} from "native-base";
import React, { Component } from "react";
import { FlatList, Image, TouchableOpacity, StyleSheet } from "react-native";
import { colors, height } from "../../assets/styles/global";
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import {
  getAllCart,
  getLatestCart,
  listItemsCart,
  incQuantity,
  decQuantity
} from "../../actions/cart";
import { getProductById } from "../../actions/product";

class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: [],
      productIds: [],
      cart_id: null
    };
    this.data = {
      user_id: props.data.id,
      uid: props.headers.uid,
      client: props.headers.client,
      token: props.headers["access-token"]
    };
  }

  async getCart() {
    try {
      setCart = await this.props.getAllCart(this.data);
      let data = setCart.data.data.slice(-1);
      let result = data[0].relationships.items.data;
      let options = this.data;
      let cart_id = data[0].id;
      this.props.getLatestCart({ cart_id, options }).then(() => {
        let id = this.props.myCart.data.data.map(data => {
          let product_id = data.attributes.product_id;
          return product_id;
          // let detailProd = this.props.products.map(item => {
          //   product_id === item.id;

          //   return detailProd;
          // });
        });
        this.setState({ productIds: id });
      });
      this.setState({ cart_id: cart_id });
      this.props.listItemsCart(result);
    } catch (error) {
      console.log("err get cart", error);
    }
  }

  componentDidMount = () => {
    this.getCart();
  };

  incButton = item => {
    let qty = item.attributes.quantity + 1;
    let data = {
      cart_id: this.state.cart_id,
      id: item.id,
      product_id: item.attributes.product_id,
      quantity: qty,
      uid: this.props.headers.uid,
      client: this.props.headers.client,
      token: this.props.headers["access-token"]
    };
    console.log("item", data);
    this.props
      .incQuantity(data)
      .then(() => {
        this.getCart();
        console.log("SUkses");
      })
      .catch(() => {
        console.log("failed");
      });
  };

  decButton = item => {
    let qty = item.attributes.quantity - 1;
    let data = {
      cart_id: this.state.cart_id,
      id: item.id,
      product_id: item.attributes.product_id,
      quantity: qty,
      uid: this.props.headers.uid,
      client: this.props.headers.client,
      token: this.props.headers["access-token"]
    };
    console.log("item", data);
    this.props
      .decQuantity(data)
      .then(() => {
        this.getCart();
        console.log("SUkses");
      })
      .catch(() => {
        console.log("failed");
      });
  };

  render() {
    return (
      <Container>
        <Header
          style={{
            backgroundColor: "#FFFFFF",
            borderBottomWidth: 0,
            borderBottomColor: "white"
          }}
        >
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>KERANJANG</H2>
          </Body>
        </Header>
        {this.props.isLoading ? (
          <Spinner color={colors.GREEN_V2} />
        ) : (
          <Content>
            <View style={{ padding: 20 }}>
              <FlatList
                ListEmptyComponent={() => {
                  return (
                    <View
                      style={{
                        width: "100%",
                        height: "100%",
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                    >
                      <Text note>Your Cart is Empty !</Text>
                    </View>
                  );
                }}
                data={this.props.myCart.data.data}
                renderItem={({ item }) => {
                  return (
                    <View style={{ marginVertical: 3 }}>
                      <View
                        style={{
                          width: "100%",
                          borderColor: "grey",
                          borderWidth: 0.5,
                          borderRadius: 5,
                          flexDirection: "row",
                          alignItems: "center"
                        }}
                      >
                        <View
                          style={{
                            width: "40%",
                            borderTopLeftRadius: 5,
                            borderBottomLeftRadius: 5
                          }}
                        >
                          <Image
                            resizeMode="cover"
                            resizeMethod="resize"
                            source={{
                              uri:
                                "https://lh6.googleusercontent.com/LW-at230jnqcN65e9he5RA6tmAJBOE9E9k62ugSjCMVSITioth1du0Kn3JmHbGt9lni9YWpj5qKVjY-eyfig=w1024-h637"
                            }}
                            style={{ width: "100%", height: 100 }}
                          />
                        </View>
                        <View
                          style={{
                            width: "60%",
                            paddingHorizontal: 15,
                            height: "100%",
                            paddingVertical: 15
                          }}
                        >
                          <Text style={{ fontSize: 20, fontWeight: "500" }}>
                            {/* {item.name} */}
                            Sin Trust
                          </Text>
                          <Text
                            style={{
                              color: colors.GREEN_V2,
                              fontWeight: "bold"
                            }}
                          >
                            {/* Rp. {item.price} */}
                            Rp. {item.attributes.total_price}
                          </Text>
                          <View
                            style={{
                              width: "100%",
                              height: 20,
                              position: "absolute",
                              right: 15,
                              bottom: 15,
                              justifyContent: "space-between",
                              flexDirection: "row"
                            }}
                          >
                            <View style={{ width: "35%" }} />
                            <View
                              style={{
                                width: "65%",
                                flexDirection: "row",
                                justifyContent: "flex-end"
                              }}
                            >
                              <TouchableOpacity style={{ marginRight: 5 }}>
                                <Icon
                                  name="trash"
                                  type="EvilIcons"
                                  style={{ color: "#E43A3A" }}
                                />
                              </TouchableOpacity>
                              <View
                                style={{
                                  borderRadius: 5,
                                  borderWidth: 0.5,
                                  flexDirection: "row",
                                  width: 100,
                                  justifyContent: "space-between",
                                  alignItems: "center"
                                }}
                              >
                                <TouchableOpacity
                                  onPress={() => this.decButton(item)}
                                  disabled={this.props.isLoading}
                                  style={{ padding: 3 }}
                                >
                                  <Icon
                                    name="minus"
                                    type="Entypo"
                                    style={{ fontSize: 12 }}
                                  />
                                </TouchableOpacity>
                                <TouchableOpacity style={{ padding: 3 }}>
                                  <Text>{item.attributes.quantity}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  disabled={this.props.isLoading}
                                  onPress={() => this.incButton(item)}
                                  style={{ padding: 3 }}
                                >
                                  <Icon
                                    name="plus"
                                    type="Entypo"
                                    style={{ fontSize: 12 }}
                                  />
                                </TouchableOpacity>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
            {this.props.myCart.data.data < 1 ? null : (
              <View>
                <View
                  style={{
                    width: "100%",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    padding: 20
                  }}
                >
                  <View>
                    <Text>SUB TOTAL</Text>
                    <Text note>Harga belum termasuk ongkir</Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        textAlign: "right",
                        fontWeight: "bold",
                        color: colors.GREEN_V2,
                        fontSize: 18
                      }}
                    >
                      Rp. 125.000
                    </Text>
                    <Text style={{ fontWeight: "bold", textAlign: "right" }}>
                      Total : {this.props.myCart.data.data.length} produk
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 12,
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: "100%",
                    flexDirection: "row",
                    padding: 12
                  }}
                >
                  <View style={{ width: "45%" }}>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("Checkout")}
                      style={{ width: "100%", justifyContent: "center" }}
                    >
                      <LinearGradient
                        start={{ x: 1, y: 0 }}
                        end={{ x: 0, y: 0 }}
                        colors={["#74B027", "#649f28", "#39712C"]}
                        style={[
                          styles.formInput,
                          {
                            height: 50,
                            justifyContent: "center",
                            alignItems: "center"
                          }
                        ]}
                      >
                        <Text style={styles.btnCheckout}>Checkout</Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  </View>
                  <View style={{ width: "45%" }}>
                    <View
                      style={{
                        width: "100%",
                        backgroundColor: "#787878",
                        borderRadius: 5
                      }}
                    >
                      <Button transparent>
                        <Text style={{ color: "white" }}>Reset Keranjang</Text>
                      </Button>
                    </View>
                  </View>
                </View>
              </View>
            )}
          </Content>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  formInput: {
    borderColor: "#39712c",
    borderRadius: 5,
    marginVertical: height * 0.01
  },
  btnCheckout: {
    marginVertical: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  }
});

const mapStateToProps = ({ cart, auth }) => ({
  isLoading: cart.isLoading,
  data: auth.userData.data.data,
  headers: auth.userData.headers,
  listCart: cart.cart,
  cart: cart.allCarts,
  listItemsCart: cart.listItemsCart,
  myCart: cart.myCart
});

const mapDispatchToProps = dispatch => ({
  getAllCart: params => dispatch(getAllCart(params)),
  getLatestCart: data => dispatch(getLatestCart(data)),
  listItemsCart: items => dispatch(listItemsCart(items)),
  getProductById: config => dispatch(getProductById(config)),
  incQuantity: item => dispatch(incQuantity(item)),
  decQuantity: item => dispatch(decQuantity(item))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
