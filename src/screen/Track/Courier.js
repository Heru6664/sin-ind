import React, { Component } from "react";
import {
  Container,
  Header,
  Button,
  Icon,
  Body,
  H2,
  Content,
  View,
  Text
} from "native-base";
import { TouchableOpacity, FlatList, StyleSheet } from "react-native";
import { RadioButton } from "react-native-paper";
import { colors } from "../../assets/styles/global";
import { connect } from "react-redux";
import { setCourier } from "../../actions/checkout";
import { StackActions, NavigationActions } from "react-navigation";

let data = [
  { id: 0, name: "JNE REG", day: "3 - 4 hari", price: "13.000", active: false },
  { id: 1, name: "JNE OKE", day: "3 - 4 hari", price: "13.000", active: false },
  {
    id: 2,
    name: "J&T EXPRESS",
    day: "3 - 4 hari",
    price: "13.000",
    active: false
  },
  { id: 3, name: "SICEPAT", day: "3 - 4 hari", price: "13.000", active: false }
];
class Courier extends Component {
  constructor() {
    super();
    this.state = {
      selected: "JNE REG"
    };
  }

  resetNav(routeName) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: routeName })]
    });
    this.props.navigation.dispatch(resetAction);
  }

  handleSetCourier = type => {
    this.setState({ selected: type });
    this.props.setCourier(type);
    this.props.navigation.navigate("Checkout");
  };

  render() {
    let { selected } = this.state;
    return (
      <Container>
        <Header
          style={{
            backgroundColor: "#FFFFFF",
            borderBottomWidth: 0,
            borderBottomColor: "white"
          }}
        >
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon style={{ color: "#74B027" }} name="close" type="EvilIcons" />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>PILIH JASA PENGIRIMAN</H2>
          </Body>
        </Header>
        <Content>
          <View
            style={{
              width: "100%",
              alignItems: "center",
              justifyContent: "center",
              paddingHorizontal: 20
            }}
          >
            {/* <FlatList
              data={data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) => {
                console.log("selected", selected);

                return ( */}

            <TouchableOpacity
              style={[
                styles.cardContainer,
                {
                  borderColor:
                    selected === "JNE REG" ? colors.GREEN_V2 : "grey",
                  borderWidth: selected === "JNE REG" ? 1 : 0.5
                }
              ]}
              onPress={() => this.handleSetCourier("JNE REG")}
            >
              <View style={styles.cardItem}>
                <Text style={styles.title}>JNE REG</Text>
                <Text note> (3 - 4 hari)</Text>
              </View>
              <View
                style={[
                  styles.cardItem,
                  { justifyContent: "space-between", width: "30%" }
                ]}
              >
                <Text style={styles.price}>Rp. 13.000</Text>
                <RadioButton
                  onPress={() => this.handleSetCourier("JNE REG")}
                  color={colors.GREEN_V2}
                  uncheckedColor={"#717171"}
                  status={selected === "JNE REG" ? "checked" : "unchecked"}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.cardContainer,
                {
                  borderColor:
                    selected === "JNE OKE" ? colors.GREEN_V2 : "grey",
                  borderWidth: selected === "JNE OKE" ? 1 : 0.5
                }
              ]}
              onPress={() => this.handleSetCourier("JNE OKE")}
            >
              <View style={styles.cardItem}>
                <Text style={styles.title}>JNE OKE</Text>
                <Text note> (3 - 4 hari)</Text>
              </View>
              <View
                style={[
                  styles.cardItem,
                  { justifyContent: "space-between", width: "30%" }
                ]}
              >
                <Text style={styles.price}>Rp. 13.000</Text>
                <RadioButton
                  onPress={() => this.handleSetCourier("JNE OKE")}
                  color={colors.GREEN_V2}
                  uncheckedColor={"#717171"}
                  status={selected === "JNE OKE" ? "checked" : "unchecked"}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.cardContainer,
                {
                  borderWidth: selected === "J&T EXPRESS" ? 1 : 0.5,
                  borderColor:
                    selected === "J&T EXPRESS" ? colors.GREEN_V2 : "grey"
                }
              ]}
              onPress={() => this.handleSetCourier("J&T EXPRESS")}
            >
              <View style={styles.cardItem}>
                <Text style={styles.title}>J&T EXPRESS</Text>
                <Text note> (3 - 4 hari)</Text>
              </View>
              <View
                style={[
                  styles.cardItem,
                  { justifyContent: "space-between", width: "30%" }
                ]}
              >
                <Text style={styles.price}>Rp. 13.000</Text>
                <RadioButton
                  onPress={() => this.handleSetCourier("J&T EXPRESS")}
                  color={colors.GREEN_V2}
                  uncheckedColor={"#717171"}
                  status={selected === "J&T EXPRESS" ? "checked" : "unchecked"}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.cardContainer,
                {
                  borderColor:
                    selected === "SICEPAT" ? colors.GREEN_V2 : "grey",
                  borderWidth: selected === "SICEPAT" ? 1 : 0.5
                }
              ]}
              onPress={() => this.handleSetCourier("SICEPAT")}
            >
              <View style={styles.cardItem}>
                <Text style={styles.title}>SICEPAT</Text>
                <Text note> (3 - 4 hari)</Text>
              </View>
              <View
                style={[
                  styles.cardItem,
                  { justifyContent: "space-between", width: "30%" }
                ]}
              >
                <Text style={styles.price}>Rp. 13.000</Text>
                <RadioButton
                  onPress={() => this.handleSetCourier("SICEPAT")}
                  color={colors.GREEN_V2}
                  uncheckedColor={"#717171"}
                  status={selected === "SICEPAT" ? "checked" : "unchecked"}
                />
              </View>
            </TouchableOpacity>
            {/* );
              }}
            /> */}
          </View>
        </Content>
      </Container>
    );
  }

  //   componentWillReceiveProps = nextProps => {
  //     console.log("willReceive", nextProps);
  //     // console.log("nextState", nextState);
  //   };

  //   shouldComponentUpdate = () => {
  //     return true;
  //   };

  //   componentWillUpdate = (nextProps, nextState) => {
  //     console.log("nextProps", nextProps);
  //     console.log("nextState", nextState);
  //   };

  //   componentDidUpdate = state => {
  //     console.log("didUpdate", state);
  //   };
}

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    margin: 15,
    borderRadius: 5,
    alignItems: "center",
    padding: 10
  },
  cardItem: {
    width: "50%",
    flexDirection: "row",
    alignItems: "center"
  },
  title: {
    fontWeight: "bold",
    fontSize: 18
  },
  price: {
    color: colors.GREEN_V2
  }
});

const mapDispatchToProps = dispatch => ({
  setCourier: params => dispatch(setCourier(params))
});

export default connect(
  null,
  mapDispatchToProps
)(Courier);
