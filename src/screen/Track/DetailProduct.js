import {
  H2,
  Icon,
  Text,
  View,
  Container,
  Content,
  Card,
  CardItem,
  H1,
  H3,
  Right,
  Left,
  Footer,
  Button,
  Body,
  Header,
  Spinner
} from "native-base";
import React, { Component } from "react";
import { TouchableOpacity, Image, FlatList, ToastAndroid } from "react-native";
import { colors, height, width } from "../../assets/styles/global";
import { connect } from "react-redux";
import StarRating from "react-native-star-rating";
import LinearGradient from "react-native-linear-gradient";
import { addItemsToCart } from "../../actions/cart";
import cart from "../../reducers/cart";

const newProduct = [
  {
    name: "Trust Menthol",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Trust",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Tru",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Tsol",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  }
];
export class DetailProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    let cart = props.currentCart.slice(-1);
    this.data = {
      cart_id: cart[0].id,
      product_id: props.detailProduct.id,
      uid: props.headers.uid,
      client: props.headers.client,
      token: props.headers["access-token"],
      quantity: 1
    };
  }

  componentDidMount = () => {
    console.log("props:", this.props);
    console.log("data", this.data);
  };
  renderAllItem({ item, index }) {
    const { name, price, rating } = item;
    return (
      <TouchableOpacity>
        <Card style={{ borderRadius: 5, marginRight: 10 }}>
          <CardItem cardBody>
            <Image
              resizeMode="cover"
              resizeMethod="resize"
              source={require("../../assets/img/bg.jpg")}
              style={{
                height: height * 0.13,
                borderTopRightRadius: 5,
                borderTopLeftRadius: 5,
                width: width * 0.38,
                flex: 1
              }}
            />
          </CardItem>
          <CardItem style={{ height: height * 0.08 }}>
            <View style={{ flexDirection: "column" }}>
              <Text>{name}</Text>
              <Text style={{ color: colors.GREEN_V2 }}>Rp. {price}</Text>
            </View>
          </CardItem>
          <CardItem style={{ backgroundColor: "transparent" }}>
            <View
              style={{
                flexDirection: "row",
                position: "absolute",
                right: 0,
                bottom: 0,
                marginBottom: 15,
                marginTop: 20,
                marginRight: 15
              }}
            >
              <StarRating
                starStyle={{ color: "#F9ED41" }}
                starSize={18}
                disabled
                maxStars={5}
                rating={rating}
              />
              <TouchableOpacity style={{ marginRight: 5 }}>
                <Icon
                  style={{ color: "grey", fontSize: 20 }}
                  name="heart"
                  type="Feather"
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon
                  style={{ color: "grey", fontSize: 20 }}
                  name="cart-outline"
                  type="MaterialCommunityIcons"
                />
              </TouchableOpacity>
            </View>
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  }

  addToCart = item => {
    this.props
      .addToCart(item)
      .then(() => {
        return ToastAndroid.showWithGravityAndOffset(
          "Berhasil masuk ke keranjang",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      })
      .catch(() => {
        return ToastAndroid.showWithGravityAndOffset(
          "Gagal masuk ke keranjang",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      });
  };

  render() {
    let {
      name,
      price,
      image,
      stock,
      description,
      rate,
      rates_count
    } = this.props.detailProduct;
    return (
      <Container>
        <Header
          style={{
            backgroundColor: "#FFFFFF",
            borderBottomWidth: 0,
            borderBottomColor: "white"
          }}
        >
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <Text style={{ fontWeight: "300" }}>DETAIL PRODUCT</Text>
          </Body>
          <View
            style={{
              marginRight: 10,
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <TouchableOpacity style={{ marginHorizontal: 15 }}>
              <Icon name="heart" type="Feather" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.state.params.navigate("Cart")
              }
              style={{ marginHorizontal: 15 }}
            >
              <Icon name="cart" type="EvilIcons" />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon name="share" type="SimpleLineIcons" />
            </TouchableOpacity>
          </View>
        </Header>
        <Content style={{ padding: 15 }}>
          <View style={{ width: "100%", height: height * 0.4 }}>
            <Image
              source={
                image
                  ? { uri: image }
                  : require("../../assets/img/empty-product.png")
              }
              style={{ height: "100%", width: "100%" }}
            />
          </View>
          <Card transparent>
            <CardItem>
              <H1>{name}</H1>
            </CardItem>
            <CardItem>
              <H3 style={{ color: colors.GREEN_V2 }}>Rp. {price}</H3>
            </CardItem>
            <CardItem>
              <View
                style={{
                  justifyContent: "space-between",
                  flexDirection: "row",
                  alignItems: "center",
                  width: "100%"
                }}
              >
                <View style={{ width: "45%" }}>
                  <View style={{ flexDirection: "row" }}>
                    <StarRating
                      starStyle={{ color: "#F9ED41" }}
                      starSize={18}
                      disabled
                      maxStars={5}
                      rating={rate || 3}
                    />
                    <Text style={{ marginLeft: 20 }}>{rates_count} Ulasan</Text>
                  </View>
                </View>
                <View style={{ width: "45%" }}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Image
                      style={{ height: 25, width: 25 }}
                      source={require("../../assets/img/stock.png")}
                      resizeMethod="auto"
                      resizeMode="contain"
                    />
                    <Text style={{ marginLeft: 15 }}>
                      {stock} Stok Tersedia
                    </Text>
                  </View>
                </View>
              </View>
            </CardItem>
            <CardItem>
              <Text>{description}</Text>
            </CardItem>
          </Card>

          <View style={{ paddingLeft: 15, marginVertical: 10 }}>
            <View
              style={{
                flexDirection: "row",
                paddingRight: 10,
                marginTop: 0,
                paddingBottom: 0
              }}
            >
              <Left>
                <H3 style={{ marginVertical: 10 }}>Lihat Produk Lainnya</H3>
              </Left>
              <Right>
                <TouchableOpacity>
                  <Text style={{ fontWeight: "bold", color: colors.GREEN_V2 }}>
                    Lihat Semua
                  </Text>
                </TouchableOpacity>
              </Right>
            </View>
            <FlatList
              style={{ paddingTop: 0, height: height * 0.3 }}
              data={newProduct}
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={this.renderAllItem.bind(this)}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </Content>
        <Footer
          style={{
            backgroundColor: "white",
            paddingHorizontal: 15,
            justifyContent: "space-between",
            alignItems: "center",
            paddingVertical: 8
          }}
        >
          <View
            style={{
              width: "35%",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              disabled={this.props.isLoading}
              onPress={() => console.log("buy now")}
              style={{ width: "100%" }}
            >
              <LinearGradient
                start={{ x: 1, y: 0 }}
                end={{ x: 0, y: 0 }}
                colors={["#74B027", "#649f28", "#39712C"]}
                style={[
                  {
                    height: 40,
                    borderRadius: 8,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                <Text style={{ color: "white" }}>Beli Sekarang</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          <Right style={{ alignItems: "center", justifyContent: "center" }}>
            <TouchableOpacity
              disabled={this.props.isLoading}
              onPress={() => this.addToCart(this.data)}
              style={{
                width: "90%",
                height: 40,
                flexDirection: "row",
                borderColor: "#74B027",
                borderWidth: 1,
                borderRadius: 8,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Image
                resizeMethod="auto"
                resizeMode="center"
                source={require("../../assets/img/addCart.png")}
              />
              {this.props.isLoading ? (
                <Spinner />
              ) : (
                <Text style={{ color: "#74B027" }}>Tambah ke keranjang</Text>
              )}
            </TouchableOpacity>
          </Right>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = ({ cart, product, auth }) => ({
  currentCart: cart.allCarts,
  detailProduct: product.detail.attributes,
  headers: auth.userData.headers,
  isLoading: cart.isLoading
});

const mapDispatchToProps = dispatch => ({
  addToCart: item => dispatch(addItemsToCart(item))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailProduct);
