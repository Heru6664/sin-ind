import React, { Component } from "react";
import { TouchableOpacity, FlatList, Image, StyleSheet } from "react-native";
import {
  Container,
  Header,
  Button,
  Icon,
  Body,
  H2,
  Content,
  View,
  Text
} from "native-base";
import { colors, height } from "../../assets/styles/global";
import LinearGradient from "react-native-linear-gradient";
import { connect } from "react-redux";

const data = [
  { name: "Sin Trust Menthol", price: "18.000", qty: 1 },
  { name: "Sin Trust Menthol", price: "18.000", qty: 1 },
  { name: "Sin Trust Menthol", price: "18.000", qty: 1 }
];

class Checkout extends Component {
  render() {
    return (
      <Container>
        <Header
          style={{
            backgroundColor: "#FFFFFF",
            borderBottomWidth: 0,
            borderBottomColor: "white"
          }}
        >
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>CHECKOUT</H2>
          </Body>
        </Header>
        <Content>
          <View style={{ borderBottomColor: "grey", borderBottomWidth: 0.5 }}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%",
                borderBottomColor: "grey",
                borderBottomWidth: 0.5,
                paddingHorizontal: 20
              }}
            >
              <View>
                <Button transparent disabled>
                  <Text>Pengiriman Ke</Text>
                </Button>
              </View>
              <View>
                <Button transparent>
                  <Text style={{ color: colors.GREEN_V2, fontWeight: "bold" }}>
                    UBAH
                  </Text>
                </Button>
              </View>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%",
                padding: 25
              }}
            >
              <View>
                <Text style={{ fontWeight: "bold" }}>Arya Aziz Baskoro</Text>
              </View>
              <View>
                <Text note>+62 87698056432</Text>
              </View>
            </View>

            <View
              style={{
                width: "100%",
                paddingHorizontal: 20,
                paddingBottom: 20
              }}
            >
              <Text note>
                Jl. Batik Kumeli No.50, Sukaluyu, Cibeunying Kaler Kota Bandung
                - Cibeunying Kaler Jawa Barat 40123
              </Text>
            </View>
          </View>

          <View style={{ padding: 20 }}>
            <FlatList
              data={data}
              renderItem={({ item }) => {
                return (
                  <View style={{ marginVertical: 3 }}>
                    <View
                      style={{
                        width: "100%",
                        borderColor: "grey",
                        borderWidth: 0.5,
                        borderRadius: 5,
                        flexDirection: "row",
                        alignItems: "center"
                      }}
                    >
                      <View
                        style={{
                          width: "40%",
                          borderTopLeftRadius: 5,
                          borderBottomLeftRadius: 5
                        }}
                      >
                        <Image
                          resizeMode="cover"
                          resizeMethod="resize"
                          source={require("../../assets/img/empty-product.png")}
                          style={{ width: "100%", height: 100 }}
                        />
                      </View>
                      <View
                        style={{
                          width: "60%",
                          paddingHorizontal: 15,
                          height: "100%",
                          paddingVertical: 15
                        }}
                      >
                        <Text style={{ fontSize: 20, fontWeight: "500" }}>
                          {item.name}
                        </Text>

                        <View
                          style={{
                            width: "100%",
                            height: 20,
                            position: "absolute",
                            right: 15,
                            bottom: 15,
                            justifyContent: "space-between",
                            flexDirection: "row"
                          }}
                        >
                          <Text
                            style={{
                              color: colors.GREEN_V2,
                              fontWeight: "bold"
                            }}
                          >
                            Rp. {item.price}
                          </Text>
                          <View
                            style={{
                              width: "65%",
                              flexDirection: "row",
                              justifyContent: "flex-end"
                            }}
                          >
                            <Text>QTY : {item.qty}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </View>
                );
              }}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Courier")}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                borderColor: "grey",
                borderWidth: 0.5,
                width: "100%",
                padding: 18,
                alignItems: "center"
              }}
            >
              <View>
                <Text>Jasa Pengiriman</Text>
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text>{this.props.courier}</Text>
                <Icon name="chevron-right" type="Entypo" />
              </View>
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View style={{ width: "45%" }} />
            <View style={{ width: "55%", paddingRight: 20, paddingTop: 20 }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Text>Ongkos Kirim</Text>
                <Text>:</Text>
                <Text style={{ fontWeight: "bold" }}> Rp. 15.000</Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Text>Sub Total</Text>
                <Text>:</Text>
                <Text style={{ fontWeight: "bold" }}> Rp. 160.000</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "space-between",
              padding: 20
            }}
          >
            <View>
              <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                TOTAL BAYAR
              </Text>
            </View>
            <View>
              <Text
                style={{
                  fontWeight: "bold",
                  color: colors.GREEN_V2,
                  fontSize: 20
                }}
              >
                Rp. 165.000
              </Text>
              <Text note>{data.length} Barang</Text>
            </View>
          </View>

          <View
            style={{ width: "100%", paddingHorizontal: 20, paddingBottom: 30 }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Checkout")}
              style={{ width: "100%", justifyContent: "center" }}
            >
              <LinearGradient
                start={{ x: 1, y: 0 }}
                end={{ x: 0, y: 0 }}
                colors={["#74B027", "#649f28", "#39712C"]}
                style={[
                  styles.formInput,
                  {
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                <Text style={styles.btnNext}>BAYAR</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  formInput: {
    borderColor: "#39712c",
    borderRadius: 5,
    marginVertical: height * 0.01
  },
  btnNext: {
    marginVertical: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  }
});

const mapStateToProps = ({ checkout }) => ({
  courier: checkout.courier
});

export default connect(mapStateToProps)(Checkout);
