import { View, Image, InteractionManager, StatusBar } from "react-native";
import React, { PureComponent } from "react";
import globalStyles, { colors } from "../../assets/styles/global";
import { connect } from "react-redux";
import { StackActions, NavigationActions } from "react-navigation";
import { Icon, H2 } from "native-base";

class SplashScreen extends PureComponent {
  static navigationOptions = {
    header: null
  };

  state = {
    ready: false
  };

  componentDidMount() {
    console.log("login?", this.props.isLogin);
    if (this.props.isLogin) {
      return this.onResetNav("Home");
    } else {
      return this.onResetNav("Login");
    }
  }

  onResetNav(routeName) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: routeName })]
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <View style={globalStyles.centerContainer}>
        <StatusBar hidden />
        <View>
          <Image
            source={require("../../assets/img/SINLogoSmall.png")}
            style={globalStyles.splashScreenLogo}
            resizeMethod="resize"
            resizeMode="contain"
          />
          <View style={{ flexDirection: "row" }}>
            <View
              style={{
                backgroundColor: colors.GREEN_V2,
                marginLeft: 15,
                paddingHorizontal: 10,
                paddingVertical: 3
              }}
            >
              <H2 style={{ color: colors.WHITE, fontWeight: "bold" }}>SIN</H2>
            </View>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <H2 style={{ marginLeft: 15, fontWeight: "bold" }}>INDONESIA</H2>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  isLogin: auth.isLogin
});

export default connect(mapStateToProps)(SplashScreen);
