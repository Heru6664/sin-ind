import { StyleSheet } from "react-native";
import { height, colors } from "../../../assets/styles/global";

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center"
  },
  form: {
    width: "85%",
    paddingVertical: 10,
    marginTop: 12
  },
  formInput: {
    borderColor: "#39712c",
    borderRadius: 5,
    marginVertical: height * 0.01
  },
  btnLogin: {
    marginVertical: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  btnSignup: {
    width: "100%",
    justifyContent: "center",
    backgroundColor: colors.WHITE,
    borderRadius: 20,
    marginVertical: height * 0.01
  },
  row: {
    width: "100%",
    textAlign: "center",
    marginVertical: height * 0.008,
    flexDirection: "row"
  },
  row2: {
    backgroundColor: colors.DARK_BLUE,
    width: "35%",
    height: 1,
    marginTop: height * 0.01
  },
  row3: {
    backgroundColor: colors.DARK_BLUE,
    width: "35%",
    height: 1,
    marginTop: height * 0.01
  },
  forgotPasswordWrapper: {
    textAlign: "right",
    fontSize: 15,
    color: "#3e7431",
    marginTop: 15,
    marginBottom: 10,
    marginRight: 10
  }
});

export default styles;
