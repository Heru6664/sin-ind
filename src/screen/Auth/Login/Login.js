import {
  Button,
  Container,
  Form,
  Text,
  View,
  Icon,
  Item,
  Input,
  Content,
  Spinner
} from "native-base";
import React, { Component } from "react";
import { connect } from "react-redux";
import { NavigationActions } from "react-navigation";
import LinearGradient from "react-native-linear-gradient";
import {
  StatusBar,
  TouchableOpacity,
  InteractionManager,
  Image
} from "react-native";
import globalStyles, { colors, height } from "../../../assets/styles/global";
import styles from "./LoginStyle";
import { loginAuth } from "../../../actions/auth";

class Login extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerStyle: { borderBottomWidth: 0, elevation: 0 },
    headerLeft: <View />,
    headerRight: (
      <View style={{ marginRight: 15 }}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Icon name="close" type="EvilIcons" />
        </TouchableOpacity>
      </View>
    )
  });

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      invalidEmail: false,
      invalidPassword: false,
      unVisible: true,
      errors: []
    };
  }

  componentDidMount = () => {
    // this.props.navigation.addListener("willFocus", () => {
    //   // StatusBar.setBackgroundColor(colors.WHITE, true);
    //   InteractionManager.runAfterInteractions(() =>
    //     this.setState({ ready: true })
    //   );
    // });
  };

  onChangeValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };

  handleError = error => {
    this.setState({ errors: error });
  };

  onPressLogin = () => {
    const { email, password } = this.state;
    if (email && password) {
      this.setState({
        invalidEmail: false,
        invalidPassword: false
      });
      this.props
        .loginAuth({ email, password })
        .then(() => {
          this.props.navigation.reset(
            [NavigationActions.navigate({ routeName: "Home" })],
            0
          );
        })
        .catch(error => {
          this.handleError(error);
        });
    } else {
      if (!email) {
        this.setState({
          invalidEmail: true
        });
      } else {
        this.setState({
          invalidEmail: false
        });
      }

      if (!password) {
        this.setState({
          invalidPassword: true
        });
      } else {
        this.setState({
          invalidPassword: false
        });
      }
    }
  };

  render() {
    const { invalidEmail, invalidPassword, unVisible } = this.state;
    return (
      <Container>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Content>
          <View style={styles.container}>
            <View
              style={{
                width: 150,
                height: height * 0.3,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Image
                resizeMethod="auto"
                resizeMode="contain"
                source={require("../../../assets/img/SINLogoSmall.png")}
                style={{ height: "100%", width: "100%" }}
              />
              <Text
                style={{
                  fontSize: 30,
                  fontWeight: "bold",
                  color: "#232323",
                  marginBottom: 15
                }}
              >
                MASUK
              </Text>
            </View>
            <View style={styles.form}>
              <Form>
                <Item regular style={styles.formInput} error={invalidEmail}>
                  <Input
                    placeholder="Email atau No. Telp"
                    autoCapitalize="none"
                    style={[{ color: invalidEmail ? "#f52740" : "grey" }]}
                    onChangeText={val => this.onChangeValue("email", val)}
                  />
                </Item>
                {invalidEmail ? (
                  <Text
                    style={[
                      globalStyles.errorWrapper,
                      globalStyles.defaultFont
                    ]}
                  >
                    Email should not be empty!
                  </Text>
                ) : null}
                <Item regular style={styles.formInput} error={invalidPassword}>
                  <Input
                    placeholder="Password"
                    returnKeyType="done"
                    autoCapitalize="none"
                    secureTextEntry={unVisible}
                    style={[{ color: invalidEmail ? "#f52740" : "grey" }]}
                    onChangeText={val => this.onChangeValue("password", val)}
                  />
                  <Icon
                    name={unVisible ? "eye" : "eye-with-line"}
                    type="Entypo"
                    onPress={() =>
                      this.setState({ unVisible: !this.state.unVisible })
                    }
                  />
                </Item>
                {invalidPassword ? (
                  <Text
                    style={[
                      globalStyles.errorWrapper,
                      globalStyles.defaultFont
                    ]}
                  >
                    Password should not be empty!
                  </Text>
                ) : null}
                <TouchableOpacity
                  disabled={this.props.isLoading}
                  onPress={() =>
                    this.props.navigation.navigate("ForgotPassword")
                  }
                >
                  <Text style={styles.forgotPasswordWrapper}>
                    Lupa Kata Sandi?
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  disabled={this.props.isLoading}
                  onPress={() => this.onPressLogin()}
                >
                  <LinearGradient
                    start={{ x: 1, y: 0 }}
                    end={{ x: 0, y: 0 }}
                    colors={["#74B027", "#649f28", "#39712C"]}
                    style={[
                      styles.formInput,
                      {
                        height: 50,
                        justifyContent: "center",
                        alignItems: "center"
                      }
                    ]}
                  >
                    {this.props.isLoading ? (
                      <Spinner color="white" />
                    ) : (
                      <Text style={styles.btnLogin}>MASUK</Text>
                    )}
                  </LinearGradient>
                </TouchableOpacity>
              </Form>
              {/* <View style={styles.row}>
                <View style={styles.row3} />
                <View style={{ width: "30%" }}>
                  <Text style={{ textAlign: "center", color: colors.BLUE_V1 }}>
                    ATAU
                  </Text>
                </View>
                <View style={styles.row2} />
              </View>
              <Button
                onPress={() => this.props.navigation.navigate("Signup")}
                disabled={this.props.isLoading}
                style={styles.btnSignup}
              >
                <Text style={{ color: colors.BLUE_V1 }}>SIGNUP</Text>
              </Button> */}
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text>Sudah punya akun? </Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Signup")}
                disabled={this.props.isLoading}
              >
                <Text style={{ color: "#39712c", fontWeight: "bold" }}>
                  Daftar
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
  isLoading: auth.isLoading,
  errorMessage: auth.errorMessage
});

const mapDispatchToProps = dispatch => ({
  loginAuth: user => dispatch(loginAuth(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
