import React, { Component } from "react";
import {
  Container,
  Content,
  View,
  Text,
  H2,
  Item,
  Input,
  Spinner
} from "native-base";
import { Image, TouchableOpacity } from "react-native";
import globalStyles, { height, colors } from "../../../assets/styles/global";
import LinearGradient from "react-native-linear-gradient";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ""
    };
  }

  onChangeValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };
  render() {
    return (
      <Container>
        <Content style={{ flex: 1 }}>
          {/* <View style={globalStyles.centerContainer}>
            <View
              style={{
                width: 150,
                height: height * 0.4,
                alignItems: "center",
                justifyContent: "center",
                marginTop: -height * 0.05
              }}
            >
              <Image
                resizeMethod="auto"
                resizeMode="contain"
                source={require("../../../assets/img/SINLogoSmall.png")}
                style={{ height: "100%", width: "100%" }}
              />
            </View>
            <Text
              style={{
                marginTop: -height * 0.05,
                fontSize: 30,
                fontWeight: "bold",
                color: "#232323",
                marginBottom: height * 0.05
              }}
            >
              LUPA KATA SANDI
            </Text>
          </View> */}

          <View style={globalStyles.centerContainer}>
            <Image
              source={require("../../../assets/img/SINLogoSmall.png")}
              style={globalStyles.splashScreenLogo}
              resizeMethod="resize"
              resizeMode="contain"
            />
            <Text
              style={{
                fontSize: 30,
                fontWeight: "bold",
                color: "#232323",
                marginBottom: height * 0.05
              }}
            >
              LUPA KATA SANDI
            </Text>
            <Text style={{ fontSize: 16, width: "60%", textAlign: "center" }}>
              Masukkan email yang tersambung dengan akun anda!
            </Text>

            <View style={{ padding: 20, width: "100%" }}>
              <Item
                regular
                style={{
                  borderColor: "#39712c",
                  borderRadius: 5,
                  marginVertical: height * 0.01
                }}
              >
                <Input
                  placeholder="Email"
                  onChangeText={val => this.onChangeValue("email", val)}
                />
              </Item>
            </View>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("NewPassword")}
              style={{ width: "100%", padding: 20 }}
              // onPress={() => this.onPressLogin()}
            >
              <LinearGradient
                start={{ x: 1, y: 0 }}
                end={{ x: 0, y: 0 }}
                colors={["#74B027", "#649f28", "#39712C"]}
                style={[
                  {
                    borderColor: "#39712c",
                    borderRadius: 5,
                    marginVertical: height * 0.01,
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                {this.props.isLoading ? (
                  <Spinner color="white" />
                ) : (
                  <Text style={{ color: "white" }}>RESET KATA SANDI</Text>
                )}
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default ForgotPassword;
