import React, { Component } from "react";
import {
  Container,
  Content,
  View,
  Text,
  H2,
  Item,
  Input,
  Spinner
} from "native-base";
import { Image, TouchableOpacity } from "react-native";
import globalStyles, { height, colors } from "../../../assets/styles/global";
import LinearGradient from "react-native-linear-gradient";

class NewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPass: "",
      reNewPass: ""
    };
  }

  onChangeValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };
  render() {
    return (
      <Container>
        <Content style={{ flex: 1 }}>
          <View style={globalStyles.centerContainer}>
            <Image
              source={require("../../../assets/img/SINLogoSmall.png")}
              style={globalStyles.splashScreenLogo}
              resizeMethod="resize"
              resizeMode="contain"
            />
            <Text
              style={{
                fontSize: 30,
                fontWeight: "bold",
                color: "#232323",
                marginBottom: height * 0.05
              }}
            >
              BUAT KATA SANDI BARU
            </Text>

            <View style={{ padding: 20, width: "100%" }}>
              <Item
                regular
                style={{
                  borderColor: "#39712c",
                  borderRadius: 5,
                  marginVertical: height * 0.01
                }}
              >
                <Input
                  placeholder="Kata Sandi Baru"
                  onChangeText={val => this.onChangeValue("newPass", val)}
                />
              </Item>

              <Item
                regular
                style={{
                  borderColor: "#39712c",
                  borderRadius: 5,
                  marginVertical: height * 0.01
                }}
              >
                <Input
                  placeholder="Ulangi Kata Sandi Baru"
                  onChangeText={val => this.onChangeValue("reNewPass", val)}
                />
              </Item>
            </View>

            <TouchableOpacity
              //   disabled={this.props.isLoading}
              onPress={() => this.props.navigation.navigate("Login")}
              style={{ width: "100%", padding: 20 }}
            >
              <LinearGradient
                start={{ x: 1, y: 0 }}
                end={{ x: 0, y: 0 }}
                colors={["#74B027", "#649f28", "#39712C"]}
                style={[
                  {
                    borderColor: "#39712c",
                    borderRadius: 5,
                    marginVertical: height * 0.01,
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                {this.props.isLoading ? (
                  <Spinner color="white" />
                ) : (
                  <Text style={{ color: "white" }}>BUAT KATA SANDI BARU</Text>
                )}
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

export default NewPassword;
