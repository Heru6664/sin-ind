import { StyleSheet } from "react-native";
import { height, colors } from "../../../assets/styles/global";

const styles = StyleSheet.create({
  form: {
    width: "85%",
    paddingVertical: 5
  },
  formInput: {
    borderColor: "#39712c",
    borderRadius: 5,
    marginVertical: height * 0.01
  },
  btnSignup: {
    width: "100%",
    marginVertical: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  btnDisable: {
    width: "100%",
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  }
});

export default styles;
