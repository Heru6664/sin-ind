import React, { Component } from "react";
import {
  Icon,
  Container,
  Content,
  Text,
  Form,
  Item,
  Input,
  Button,
  Spinner
} from "native-base";
import { View, TouchableOpacity, Alert, StatusBar, Image } from "react-native";
import { CheckBox } from "react-native-elements";
import globalStyles, { height, colors } from ":../../../assets/styles/global";
import styles from "./SignupStyle";
import { connect } from "react-redux";
import { signUpUser } from "../../../actions/signup";
import LinearGradient from "react-native-linear-gradient";

export class Signup extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      passwordConfirmation: "",
      phone: "",
      terms: false,
      errors: []
    };
  }

  onChangeValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };

  handleError = error => {
    this.setState({ errors: error });
  };

  handleSignUp = () => {
    const {
      firstName,
      lastName,
      email,
      password,
      passwordConfirmation,
      phone
    } = this.state;
    this.props
      .signUpUser({
        firstName,
        lastName,
        email,
        password,
        passwordConfirmation,
        phone
      })
      .then(() => {
        Alert.alert("Signup Successfully");
        this.props.navigation.navigate("Login");
      })
      .catch(error => {
        return this.handleError(error);
      });
  };

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Content>
          <View style={globalStyles.centerContainer}>
            <View
              style={{
                width: 150,
                height: height * 0.4,
                alignItems: "center",
                justifyContent: "center",
                marginTop: -height * 0.05
              }}
            >
              <Image
                resizeMethod="auto"
                resizeMode="contain"
                source={require("../../../assets/img/SINLogoSmall.png")}
                style={{ height: "100%", width: "100%" }}
              />
              <Text
                style={{
                  marginTop: -height * 0.05,
                  fontSize: 30,
                  fontWeight: "bold",
                  color: "#232323",
                  marginBottom: height * 0.05
                }}
              >
                DAFTAR
              </Text>
            </View>
            <View
              style={{
                width: "85%",
                paddingVertical: 5
              }}
            >
              <Form>
                <Item regular style={styles.formInput}>
                  <Input
                    placeholder="First Name"
                    onChangeText={val => this.onChangeValue("firstName", val)}
                  />
                </Item>
                <Item regular style={styles.formInput}>
                  <Input
                    placeholder="Last Name"
                    onChangeText={val => this.onChangeValue("lastName", val)}
                  />
                </Item>
                <Item regular style={styles.formInput}>
                  <Input
                    placeholder="Email"
                    autoCapitalize="none"
                    onChangeText={val => this.onChangeValue("email", val)}
                  />
                </Item>
                <Item regular style={styles.formInput}>
                  <Input
                    placeholder="Password"
                    secureTextEntry={true}
                    onChangeText={val => this.onChangeValue("password", val)}
                  />
                </Item>
                <Item regular style={styles.formInput}>
                  <Input
                    placeholder="Re-Type Password"
                    secureTextEntry={true}
                    onChangeText={val =>
                      this.onChangeValue("passwordConfirmation", val)
                    }
                  />
                </Item>
                <Item regular style={styles.formInput}>
                  <Text style={{ color: "#39712c", marginLeft: 15 }}>+62</Text>
                  <Input
                    keyboardType="numeric"
                    maxLength={13}
                    onChangeText={val => this.onChangeValue("phone", val)}
                  />
                </Item>
                <CheckBox
                  title="Saya bersedia Lorem ipsum dolor sit amet, consectetur adipiscing"
                  checked={this.state.terms}
                  onPress={() => this.setState({ terms: !this.state.terms })}
                />
                <TouchableOpacity
                  disabled={!this.state.terms}
                  onPress={() => this.handleSignUp()}
                  style={[
                    styles.formInput,
                    !this.state.terms ? styles.btnDisable : styles.btnLogin
                  ]}
                >
                  <LinearGradient
                    start={{ x: 1, y: 0 }}
                    end={{ x: 0, y: 0 }}
                    colors={
                      !this.state.terms
                        ? ["#71aa9e", "#45a792", "#147b66"]
                        : ["#74B027", "#649f28", "#39712C"]
                    }
                    style={{
                      height: 50,
                      borderRadius: 20,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    {this.props.isLoading ? (
                      <Spinner color="white" />
                    ) : (
                      <Text style={styles.btnSignup}>SIGNUP</Text>
                    )}
                  </LinearGradient>
                </TouchableOpacity>
              </Form>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ auth, signup }) => ({
  isLoading: signup.loading,
  errorMessage: auth.errorMessage
});

const mapDispatchToProps = dispatch => ({
  signUpUser: user => dispatch(signUpUser(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Signup);
