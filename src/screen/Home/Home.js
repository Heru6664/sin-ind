import { Container } from "native-base";
import React, { Component } from "react";
import { StatusBar } from "react-native";
import FooterTab from "../../assets/Components/FooterTab/";

class home extends Component {
  render() {
    return (
      <Container>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <FooterTab screenProps={this.props.screenProps} />
      </Container>
    );
  }
}

export default home;
