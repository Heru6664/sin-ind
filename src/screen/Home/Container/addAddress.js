import React, { Component } from "react";
import { StatusBar, StyleSheet, TouchableOpacity } from "react-native";
import {
  Container,
  Header,
  Button,
  Icon,
  Body,
  H2,
  Content,
  View,
  Item,
  Text,
  Input
} from "native-base";
import globalStyles, { height } from "../../../assets/styles/global";
import { CheckBox } from "react-native-elements";
import LinearGradient from "react-native-linear-gradient";

class AddAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      receiverName: "",
      fullAddress: "",
      province: "",
      city: "",
      district: "",
      zipCode: "",
      phone: "",
      primaryAddr: false
    };
  }
  onChangeValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Header style={{ backgroundColor: "#FFFFFF", borderBottomWidth: 0 }}>
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>TAMBAH ALAMAT BARU</H2>
          </Body>
        </Header>
        <Content>
          <View style={globalStyles.centerContainer}>
            <View style={{ width: "85%" }}>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="Nama Penerima"
                  onChangeText={val => this.onChangeValue("receiverName", val)}
                />
              </Item>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="Alamat Lengkap"
                  onChangeText={val => this.onChangeValue("fullAddress", val)}
                />
              </Item>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="Provinsi"
                  onChangeText={val => this.onChangeValue("province", val)}
                />
              </Item>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="Kota / Kabupaten"
                  onChangeText={val => this.onChangeValue("city", val)}
                />
              </Item>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="Kelurahan"
                  onChangeText={val => this.onChangeValue("district", val)}
                />
              </Item>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="Kode Pos"
                  onChangeText={val => this.onChangeValue("zipCode", val)}
                />
              </Item>
              <Item regular style={styles.formInput}>
                <Input
                  placeholder="No. Telepon"
                  onChangeText={val => this.onChangeValue("phone", val)}
                />
              </Item>
              <CheckBox
                title="Simpan sebagai alamat utama ?"
                checked={this.state.primaryAddr}
                onPress={() =>
                  this.setState({ primaryAddr: !this.state.primaryAddr })
                }
              />
            </View>
            <TouchableOpacity style={{ width: "85%", marginBottom: 50 }}>
              <LinearGradient
                start={{ x: 1, y: 0 }}
                end={{ x: 0, y: 0 }}
                colors={["#74B027", "#649f28", "#39712C"]}
                style={[
                  styles.formInput,
                  {
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                <Text style={styles.btnLogin}>SIMPAN</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  formInput: {
    borderColor: "#39712c",
    borderRadius: 5,
    marginVertical: height * 0.01
  },
  btnLogin: {
    marginVertical: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  }
});

export default AddAddress;
