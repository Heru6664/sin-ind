import {
  Body,
  Button,
  Card,
  CardItem,
  Container,
  Content,
  H2,
  Header,
  Icon,
  Text,
  View
} from "native-base";
import React, { PureComponent } from "react";
import { FlatList, Image, TouchableOpacity } from "react-native";
import StarRating from "react-native-star-rating";
import { colors, height, width } from "../../../assets/styles/global";

const newProduct = [
  {
    name: "Trust Menthol",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Trust",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Tru",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  }
];

class Wishlist extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }
  renderAllItem({ item, index }) {
    const { name, price, rating } = item;
    return (
      <TouchableOpacity
        onPress={() => this.getDetail(item)}
        style={{ width: "50%" }}
      >
        <Card style={{ borderRadius: 5, marginRight: 10 }}>
          <CardItem cardBody>
            <Image
              resizeMode="cover"
              resizeMethod="resize"
              source={require("../../../assets/img/bg.jpg")}
              style={{
                height: height * 0.13,
                borderTopRightRadius: 5,
                borderTopLeftRadius: 5,
                width: width * 0.38,
                flex: 1
              }}
            />
          </CardItem>
          <CardItem style={{ height: height * 0.08 }}>
            <View style={{ flexDirection: "column" }}>
              <Text>{name}</Text>
              <Text style={{ color: colors.GREEN_V2 }}>Rp. {price}</Text>
            </View>
          </CardItem>
          <CardItem style={{ backgroundColor: "transparent" }}>
            <View
              style={{
                position: "absolute",
                left: 0,
                bottom: 0,
                marginBottom: 15,
                marginTop: 20,
                marginLeft: 15
              }}
            >
              <StarRating
                starStyle={{
                  color: "#F9ED41"
                }}
                starSize={18}
                disabled
                maxStars={5}
                rating={rating}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                position: "absolute",
                right: 0,
                bottom: 0,
                marginBottom: 15,
                marginTop: 20,
                marginRight: 15
              }}
            >
              <TouchableOpacity style={{ marginRight: 5 }}>
                <Icon
                  style={{ color: "grey", fontSize: 20 }}
                  name="heart"
                  type="Feather"
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon
                  style={{ color: "grey", fontSize: 20 }}
                  name="cart-outline"
                  type="MaterialCommunityIcons"
                />
              </TouchableOpacity>
            </View>
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <Container>
        <Header
          hasTabs
          style={{ backgroundColor: "#FFFFFF", borderBottomWidth: 0 }}
        >
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>PRODUK WISHLIST</H2>
          </Body>
        </Header>
        <Content style={{ padding: 10 }}>
          <View style={{ paddingBottom: 20 }}>
            <FlatList
              style={{ paddingTop: 0, height: "100%" }}
              data={newProduct}
              numColumns={2}
              renderItem={this.renderAllItem.bind(this)}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </Content>
      </Container>
    );
  }
}

export default Wishlist;
