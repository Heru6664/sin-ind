import React, { Component } from "react";
import { Image, StyleSheet, TouchableOpacity, StatusBar } from "react-native";
import {
  Container,
  Header,
  Button,
  Icon,
  Body,
  H2,
  Content,
  View,
  Text,
  Input,
  Item
} from "native-base";
import globalStyles, {
  width,
  colors,
  height,
  Metrics
} from "../../../assets/styles/global";
import RadioGroup from "react-native-radio-buttons-group";
import LinearGradient from "react-native-linear-gradient";
import ImagePicker from "react-native-image-picker";
// import ImagePicker from "react-native-image-crop-picker";
import { connect } from "react-redux";
import { editUser } from "../../../actions/users";

class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          label: "Perempuan"
        },
        {
          label: "Laki - Laki"
        }
      ],
      default:
        "http://keenthemes.com/preview/metronic/theme/assets/pages/media/profile/profile_user.jpg",

      img: null,
      id: props.userData.id,
      selectedImage: null,
      username: props.userData.username,
      email: props.userData.email,
      firstName: props.userData.first_name,
      lastName: props.userData.last_name,
      telephone: props.userData.telephone,
      uid: props.headers.uid,
      client: props.headers.client,
      token: props.headers["access-token"]
    };
  }

  openPicker = () => {
    const options = {
      title: "Select Avatar",
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };

    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          img: response,
          selectedImage: response.uri
        });
      }
    });
  };

  openImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 300,
      cropping: true,
      mediaType: "photo",
      loadingLabelText: "Uploading Image"
    })
      .then(image => {
        console.log(image);
        this.props.dispatch(uploadImage(image));
      })
      .catch(error => {
        console.log(error);
      });
  };

  onChangeValue = (key, val) => {
    this.setState({
      [key]: val
    });
  };

  onPress = data => this.setState({ data });

  render() {
    let selectedButton = this.state.data.find(e => e.selected == true);
    selectedButton = selectedButton
      ? selectedButton.value
      : this.state.data[0].label;
    return (
      <Container>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Header style={{ backgroundColor: "#FFFFFF", borderBottomWidth: 0 }}>
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>EDIT PROFILE</H2>
          </Body>
        </Header>
        <Content>
          <View style={globalStyles.centerContainer}>
            <TouchableOpacity onPress={() => this.openPicker()}>
              <View style={styles.imgContainer}>
                <Image
                  style={styles.profileImg}
                  resizeMode="center"
                  source={{
                    uri: this.state.selectedImage || this.state.default,
                    height: "100%",
                    width: "100%"
                  }}
                />
              </View>
              <Text style={{ color: colors.GREEN_V2, fontWeight: "bold" }}>
                UBAH FOTO PROFILE
              </Text>
            </TouchableOpacity>
            {/*  */}
            <View style={styles.inputView}>
              <View style={styles.textInput}>
                <View style={styles.inputTitle}>
                  <Text style={{ color: colors.GREEN_V2 }}>Username</Text>
                </View>
                <Item>
                  <Input
                    onChangeText={val => this.onChangeValue("username", val)}
                    defaultValue={this.props.userData.username}
                  />
                </Item>
              </View>
            </View>
            {/*  */}
            <View style={styles.inputView}>
              <View style={styles.textInput}>
                <View style={styles.inputTitle}>
                  <Text style={{ color: colors.GREEN_V2 }}>Email</Text>
                </View>
                <Item>
                  <Input
                    onChangeText={val => this.onChangeValue("email", val)}
                    defaultValue={this.props.userData.email}
                  />
                </Item>
              </View>
            </View>
            {/*  */}
            <View style={styles.inputView}>
              <View style={styles.textInput}>
                <View style={styles.inputTitle}>
                  <Text style={{ color: colors.GREEN_V2 }}>Nama Depan</Text>
                </View>
                <Item>
                  <Input
                    onChangeText={val => this.onChangeValue("firstName", val)}
                    defaultValue={this.props.userData.first_name}
                  />
                </Item>
              </View>
            </View>
            {/*  */}
            <View style={styles.inputView}>
              <View style={styles.textInput}>
                <View style={styles.inputTitle}>
                  <Text style={{ color: colors.GREEN_V2 }}>Nama Belakang</Text>
                </View>
                <Item>
                  <Input
                    onChangeText={val => this.onChangeValue("lastName", val)}
                    defaultValue={this.props.userData.last_name}
                  />
                </Item>
              </View>
            </View>
            {/*  */}
            <View>
              <View>
                <Text>Jenis Kelamin</Text>
              </View>
              <View style={{ width: "60%" }}>
                <RadioGroup
                  radioButtons={this.state.data}
                  onPress={this.onPress}
                  flexDirection="row"
                />
              </View>
            </View>
            {/*  */}
            <View style={[styles.inputView, { marginBottom: 20 }]}>
              <View style={styles.textInput}>
                <View style={styles.inputTitle}>
                  <Text style={{ color: colors.GREEN_V2 }}>No. Telepon</Text>
                </View>
                <Item>
                  <Text note>+62</Text>
                  <Input
                    maxLength={13}
                    keyboardType="number-pad"
                    onChangeText={val => this.onChangeValue("telephone", val)}
                    defaultValue={this.props.userData.telephone}
                  />
                </Item>
              </View>
            </View>

            <TouchableOpacity
              onPress={() => this.props.editProfile(this.state)}
              style={{ width: "85%", marginBottom: 50 }}
            >
              <LinearGradient
                start={{ x: 1, y: 0 }}
                end={{ x: 0, y: 0 }}
                colors={["#74B027", "#649f28", "#39712C"]}
                style={[
                  styles.formInput,
                  {
                    height: 50,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                ]}
              >
                <Text style={styles.btnLogin}>SIMPAN</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  imgContainer: {
    width: 150,
    height: 150,
    padding: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  profileImg: {
    width: "100%",
    height: "100%",
    borderRadius: 100
  },
  inputView: {
    width: "85%",
    marginTop: height * 0.03
  },
  textInput: {
    borderWidth: 1,
    borderColor: colors.GREEN_V2,
    borderRadius: 5,
    paddingLeft: 15
  },
  inputTitle: {
    backgroundColor: "white",
    position: "absolute",
    top: -20,
    left: 8,
    padding: 10
  },
  formInput: {
    borderColor: "#39712c",
    borderRadius: 5,
    marginVertical: height * 0.01
  },
  btnLogin: {
    marginVertical: 15,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  }
});

const mapStateToProps = ({ auth }) => ({
  headers: auth.userData.headers,
  userData: auth.userData.data.data
});

const mapDispatchToProps = dispatch => ({
  editProfile: data => dispatch(editUser(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);
