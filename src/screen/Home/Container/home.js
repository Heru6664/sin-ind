import {
  Card,
  CardItem,
  Container,
  Content,
  H2,
  H3,
  Header,
  Icon,
  Input,
  Item,
  Left,
  Right,
  Text,
  View,
  Spinner
} from "native-base";
import React, { PureComponent } from "react";
import { StackActions, NavigationActions } from "react-navigation";
import {
  FlatList,
  Image,
  ImageBackground,
  InteractionManager,
  StatusBar,
  TouchableOpacity
} from "react-native";
import StarRating from "react-native-star-rating";
import { connect } from "react-redux";
import { getDetail, getProducts } from "../../../actions/product";
import { signOut } from "../../../actions/auth";
import globalStyles, {
  colors,
  height,
  width
} from "../../../assets/styles/global";
import {
  getAllCart,
  getLatestCart,
  listItemsCart
} from "../../../actions/cart";

const newProduct = [
  {
    name: "Trust Menthol",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Trust",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Tru",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  },
  {
    name: "Tsol",
    price: "18.000",
    image: "image",
    type: "Entypo",
    srcImg: "../../../assets/img/bg.jpg",
    rating: 3
  }
];
class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      uid: props.headers.uid,
      client: props.headers.client,
      token: props.headers["access-token"],
      allProducts: [],
      productIds: []
    };
    this.data = {
      user_id: props.user.id,
      uid: props.headers.uid,
      client: props.headers.client,
      token: props.headers["access-token"]
    };
  }

  async getCart() {
    try {
      setCart = await this.props.getAllCart(this.data);
      let data = setCart.data.data.slice(-1);
      let result = data[0].relationships.items.data;
      let options = this.data;
      let cart_id = data[0].id;

      this.props.getLatestCart({ cart_id, options }).then(() => {
        let id = this.props.myCart.data.data.map(data => {
          let product_id = data.attributes.product_id;
          return product_id;
          // let detailProd = this.props.products.map(item => {
          //   product_id === item.id;

          //   return detailProd;
          // });
        });
        this.setState({ productIds: id });
      });

      this.props.listItemsCart(result);
    } catch (error) {
      console.log("err get cart", error);
    }
  }

  componentDidMount() {
    this.props
      .getProducts(this.state)
      .then(res => console.log("props.product", this.props.products));

    InteractionManager.runAfterInteractions(() => {
      this.setState({ ready: true });
    });
    this.getCart();
  }

  onResetNav(routeName) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: routeName })]
    });
    // this.props
    //   .logout(this.state)
    //   .then(() =>
    this.props.screenProps.dispatch(resetAction);
    // );
  }

  getDetail = item => {
    this.props.getDetail(item);
    this.props.screenProps.navigate("DetailProduct", this.props.navigation);
  };

  renderItem({ item, index }) {
    const { name, price, rate, image } = item.attributes;
    return (
      <TouchableOpacity
        onPress={() => this.getDetail(item)}
        underlayColor={"transparent"}
        style={[
          globalStyles.cardContainer,

          {
            marginTop: height * 0.01,
            marginRight: index == 4 ? height * 0.05 : 0,
            elevation: 0
          }
        ]}
      >
        <ImageBackground
          borderRadius={5}
          resizeMethod="scale"
          resizeMode="stretch"
          source={require("../../../assets/img/Surface.png")}
          style={{
            flex: 1,
            borderRadius: 5,
            width: "100%",
            height: "100%",
            justifyContent: "space-between"
          }}
        >
          <View
            style={{
              position: "absolute",
              top: 0,
              right: 0,
              bottom: 0,
              left: 0,
              backgroundColor: "rgba(0,0,0,0.5)",
              borderRadius: 5,
              padding: 15,
              justifyContent: "space-between"
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Right>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Cart")}
                >
                  <Icon
                    name="shopping-cart"
                    type="Feather"
                    style={{ color: "white", marginRight: 20, fontSize: 22 }}
                  />
                </TouchableOpacity>
              </Right>
              <TouchableOpacity>
                <Icon
                  name="heart"
                  type="Feather"
                  style={{ color: "white", fontSize: 18 }}
                />
              </TouchableOpacity>
            </View>
            <View style={{ marginTop: 25 }}>
              <Text
                style={{ color: "white", fontSize: 18, fontWeight: "bold" }}
              >
                {name}
              </Text>
              <Text style={{ color: "white" }}>Rp. {price}</Text>
            </View>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  }

  renderAllItem({ item, index }) {
    const { name, price, rate, image } = item.attributes;
    return (
      <TouchableOpacity onPress={() => this.getDetail(item)}>
        <Card style={{ borderRadius: 5, marginRight: 10 }}>
          <CardItem cardBody>
            <Image
              resizeMode="cover"
              resizeMethod="resize"
              source={
                image
                  ? { uri: image }
                  : require("../../../assets/img/smartnew.png")
              }
              style={{
                height: height * 0.13,
                borderTopRightRadius: 5,
                borderTopLeftRadius: 5,
                width: width * 0.38,
                flex: 1
              }}
            />
          </CardItem>
          <CardItem style={{ height: height * 0.08 }}>
            <View style={{ flexDirection: "column" }}>
              <Text>{name}</Text>
              <Text style={{ color: colors.GREEN_V2 }}>Rp. {price}</Text>
            </View>
          </CardItem>
          <CardItem style={{ backgroundColor: "transparent" }}>
            <View
              style={{
                flexDirection: "row",
                position: "absolute",
                right: 0,
                bottom: 0,
                marginBottom: 15,
                marginTop: 20,
                marginRight: 15
              }}
            >
              <StarRating
                starStyle={{ color: "#F9ED41" }}
                starSize={18}
                disabled
                maxStars={5}
                rating={rate || 3}
              />
              <TouchableOpacity style={{ marginRight: 5 }}>
                <Icon
                  style={{ color: "grey", fontSize: 20 }}
                  name="heart"
                  type="Feather"
                />
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon
                  style={{ color: "grey", fontSize: 20 }}
                  name="cart-outline"
                  type="MaterialCommunityIcons"
                />
              </TouchableOpacity>
            </View>
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  }

  renderGalery({ item, index }) {
    return (
      <TouchableOpacity>
        <Card style={{ borderRadius: 5, marginRight: 10 }}>
          <CardItem cardBody>
            <Image
              resizeMode="cover"
              resizeMethod="resize"
              source={require("../../../assets/img/bg.jpg")}
              style={{
                height: height * 0.2,
                borderTopRightRadius: 5,
                borderTopLeftRadius: 5,
                borderBottomLeftRadius: 5,
                borderBottomRightRadius: 5,
                width: width * 0.38,
                flex: 1
              }}
            />
          </CardItem>
        </Card>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: "white" }}>
          <StatusBar backgroundColor="white" barStyle="dark-content" />
          <View style={{ justifyContent: "center" }}>
            <View style={{ flexDirection: "row" }}>
              <View
                style={{
                  backgroundColor: colors.GREEN_V2,
                  marginLeft: 15,
                  paddingHorizontal: 10,
                  paddingVertical: 3
                }}
              >
                <H2 style={{ color: colors.WHITE, fontWeight: "bold" }}>SIN</H2>
              </View>
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <H2 style={{ marginLeft: 15, fontWeight: "bold" }}>
                  INDONESIA
                </H2>
              </View>
            </View>
          </View>
          <Right>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Cart")}
              >
                <Icon
                  name="shopping-cart"
                  type="Feather"
                  style={{ color: colors.GREEN_V2, fontSize: 20 }}
                />
              </TouchableOpacity>
              <TouchableOpacity style={{ marginHorizontal: 20 }}>
                <Icon
                  name="notifications-none"
                  type="MaterialIcons"
                  style={{ color: colors.GREEN_V2, fontSize: 20 }}
                />
              </TouchableOpacity>
            </View>
          </Right>
        </Header>
        {this.props.loadingProduct ? (
          <Spinner color={colors.GREEN_V2} />
        ) : this.props.failed ? (
          <Content style={{ backgroundColor: "rgba(0,0,0,0.2)" }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Card style={{ justifyContent: "center", alignItems: "center" }}>
                <CardItem>
                  <Text>Sorry :( Failed to get data</Text>
                </CardItem>
                <CardItem>
                  <Text>
                    Please
                    <Text
                      onPress={() => this.onResetNav("Login")}
                      style={{ fontWeight: "bold", color: colors.GREEN_V2 }}
                    >
                      {" "}
                      Login Again
                    </Text>
                  </Text>
                </CardItem>
              </Card>
            </View>
          </Content>
        ) : (
          <Content style={{ paddingTop: 10 }}>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <View style={{ width: "92%", height: 35 }}>
                <Item rounded style={{ borderRadius: 5 }}>
                  <Icon
                    style={{ color: colors.GREEN_V2, fontSize: 30 }}
                    name="search"
                    type="EvilIcons"
                  />
                  <Input placeholder="Cari Produk" />
                </Item>
              </View>
            </View>

            <View style={{ paddingLeft: 15 }}>
              <View
                style={{
                  flexDirection: "row",
                  paddingRight: 10,
                  marginTop: height * 0.06,
                  paddingBottom: 0
                }}
              >
                <Left>
                  <H3>PRODUK TERBARU</H3>
                </Left>
                <Right>
                  <TouchableOpacity>
                    <Text
                      style={{ fontWeight: "bold", color: colors.GREEN_V2 }}
                    >
                      Lihat Semua
                    </Text>
                  </TouchableOpacity>
                </Right>
              </View>
              <FlatList
                style={{
                  paddingTop: 0,
                  height: height * 0.23,
                  marginBottom: 12
                }}
                data={this.props.products.slice(
                  Math.max(this.props.products.length - 4, 1)
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={this.renderItem.bind(this)}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>

            <View style={{ paddingLeft: 15, marginTop: 0 }}>
              <View
                style={{
                  flexDirection: "row",
                  paddingRight: 10,
                  marginTop: 0,
                  paddingBottom: 0
                }}
              >
                <Left>
                  <H3>SEMUA PRODUK</H3>
                </Left>
                <Right>
                  <TouchableOpacity>
                    <Text
                      style={{ fontWeight: "bold", color: colors.GREEN_V2 }}
                    >
                      Lihat Semua
                    </Text>
                  </TouchableOpacity>
                </Right>
              </View>
              <FlatList
                style={{ paddingTop: 0, height: height * 0.3 }}
                data={this.props.products.slice(0, 4)}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={this.renderAllItem.bind(this)}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>

            <View style={{ paddingLeft: 15 }}>
              <View
                style={{
                  flexDirection: "row",
                  paddingRight: 10
                }}
              >
                <Left>
                  <H3>UPDATE GALERY</H3>
                </Left>
                <Right>
                  <TouchableOpacity>
                    <Text
                      style={{ fontWeight: "bold", color: colors.GREEN_V2 }}
                    >
                      Lihat Semua
                    </Text>
                  </TouchableOpacity>
                </Right>
              </View>
              <FlatList
                style={{ paddingTop: 0, height: height * 0.3 }}
                data={newProduct}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={this.renderGalery.bind(this)}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </Content>
        )}
      </Container>
    );
  }
}

const mapStateToProps = ({ auth, product, cart }) => ({
  user: auth.userData.data.data,
  headers: auth.userData.headers,
  loadingProduct: product.isLoading || cart.isLoading,
  products: product.products,
  failed: product.failed || cart.failed,
  myCart: cart.myCart
});

const mapDispatchToProps = dispatch => ({
  getDetail: detail => dispatch(getDetail(detail)),
  getProducts: data => dispatch(getProducts(data)),
  logout: reset => dispatch(signOut(reset)),
  getAllCart: params => dispatch(getAllCart(params)),
  getLatestCart: data => dispatch(getLatestCart(data)),
  listItemsCart: items => dispatch(listItemsCart(items))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
