import React, { PureComponent } from "react";
import { Alert, Image, TouchableOpacity, StyleSheet } from "react-native";
import {
  Container,
  Content,
  View,
  Text,
  Form,
  Input,
  Item,
  Label,
  Button,
  Header,
  Right,
  Icon,
  Body,
  H2,
  Left
} from "native-base";
import { StackActions, NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { colors, width, height } from "../../../assets/styles/global";
import { editUser } from "../../../actions/users";
import { signOut } from "../../../actions/auth";

class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      editMode: false,
      id: props.user.id,
      firstName: props.user.first_name,
      lastName: props.user.last_name,
      userName: props.user.username,
      email: props.user.email,
      telephone: props.user.telephone,
      longitude: parseFloat(props.user["longitude"]),
      latitude: parseFloat(props.user["latitude"]),
      about: props.user.about,
      avatar: props.user.avatar,
      uid: props.header.uid,
      client: props.header.client,
      token: props.header["access-token"]
    };
  }

  componentDidMount() {
    console.log("this2:", this);
  }

  onLogout = () => {
    Alert.alert(
      "Logout",
      "Are you sure?",
      [
        {
          text: "Yes",
          onPress: () =>
            this.props.logout(this).then(res => {
              console.log("logout success");
            })
        },
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  };

  resetNav(routeName) {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: routeName })]
    });
    this.props.screenProps.dispatch(resetAction);
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: "white" }}>
          <Body>
            <H2 style={{ fontWeight: "300" }}>PROFILE SAYA</H2>
          </Body>
          <Right>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity>
                <Icon
                  name="shopping-cart"
                  type="Feather"
                  style={{ color: colors.GREEN_V2, fontSize: 20 }}
                />
              </TouchableOpacity>
              <TouchableOpacity style={{ marginHorizontal: 20 }}>
                <Icon
                  name="notifications-none"
                  type="MaterialIcons"
                  style={{ color: colors.GREEN_V2, fontSize: 20 }}
                />
              </TouchableOpacity>
            </View>
          </Right>
        </Header>
        <Content style={{ paddingVertical: 25 }}>
          <View style={{ justifiContent: "center", alignItems: "center" }}>
            <View style={styles.contentTop}>
              <View style={styles.imgContainer}>
                <TouchableOpacity>
                  <Image
                    resizeMode="cover"
                    source={
                      this.props.user.avatar.url == null
                        ? require("../../../assets/img/bg.jpg")
                        : { uri: this.props.user.avatar.url }
                    }
                    style={styles.profileImg}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.dataProfile}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("EditProfile")}
                  style={{
                    position: "absolute",
                    right: 12,
                    top: 0
                  }}
                >
                  <Text style={{ color: colors.GREEN_V2, fontWeight: "500" }}>
                    Ubah Profile
                  </Text>
                </TouchableOpacity>
                <Text style={{ fontSize: 24, fontWeight: "bold" }}>
                  {this.props.user.first_name + " " + this.props.user.last_name}
                </Text>
                <Text style={{ color: colors.GREEN_V2 }}>
                  username : {this.props.user.username}
                </Text>
                <Text>Bergabung sejak : 29 Desember 2018</Text>
              </View>
            </View>
          </View>
          <View style={{ padding: 25 }}>
            <TouchableOpacity>
              <View style={styles.listItem}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon name="bag" type="SimpleLineIcons" />
                  <Text style={{ marginLeft: 20 }}>Belanjaan Saya</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text note>Lihat Riwayat Order</Text>
                  <Icon name="arrow-right" type="SimpleLineIcons" />
                </View>
              </View>
            </TouchableOpacity>
            {/*  */}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Wishlist")}
            >
              <View style={styles.listItem}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon name="heart" type="Entypo" style={{ color: "red" }} />
                  <Text style={{ marginLeft: 20 }}>Produk Wishlist</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text note>5 produk</Text>
                  <Icon name="arrow-right" type="SimpleLineIcons" />
                </View>
              </View>
            </TouchableOpacity>
            {/*  */}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Address")}
            >
              <View style={styles.listItem}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon
                    name="location-pin"
                    type="SimpleLineIcons"
                    style={{ color: "blue" }}
                  />
                  <Text style={{ marginLeft: 20 }}>Pengaturan Alamat</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text note>Alamat Belum Diatur</Text>
                  <Icon name="arrow-right" type="SimpleLineIcons" />
                </View>
              </View>
            </TouchableOpacity>
            {/*  */}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("ChangePassword")}
            >
              <View style={styles.listItem}>
                <Left style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon name="lock-outline" type="MaterialIcons" />
                  <Text style={{ marginLeft: 20 }}>Ubah Kata Sandi</Text>
                </Left>
                <Body />
              </View>
            </TouchableOpacity>
            {/*  */}
            <TouchableOpacity>
              <View style={styles.listItem}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon name="info" type="SimpleLineIcons" />
                  <Text style={{ marginLeft: 20 }}>Bantuan</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text note>Lihat Bantuan</Text>
                  <Icon name="arrow-right" type="SimpleLineIcons" />
                </View>
              </View>
            </TouchableOpacity>
            {/*  */}

            <TouchableOpacity onPress={() => this.onLogout()}>
              <View style={styles.listItem}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Icon name="logout" type="MaterialCommunityIcons" />
                  <Text style={{ marginLeft: 20 }}>Keluar</Text>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Text note>Keluar Dari Aplikasi</Text>
                  <Icon name="arrow-right" type="SimpleLineIcons" />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  contentTop: {
    width: "90%",
    alignItems: "center",
    borderWidth: 0.5,
    borderColor: "grey",
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: 20
  },
  imgContainer: {
    width: width * 0.3,
    padding: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  profileImg: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100
  },
  dataProfile: {
    width: "70%",
    height: 115,
    paddingLeft: 30,
    paddingTop: 12,
    justifyContent: "space-between"
  },
  listItem: {
    marginVertical: 12,
    flexDirection: "row",
    borderBottomColor: "grey",
    borderBottomWidth: 0.5,
    paddingTop: 5,
    paddingBottom: 10,
    justifyContent: "space-between"
  }
});

const mapStateToProps = ({ auth }) => ({
  user: auth.userData.data.data,
  header: auth.userData.headers
});

const mapDispatchToProps = dispatch => ({
  saveChange: data => dispatch(editUser(data)),
  logout: reset => dispatch(signOut(reset))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
