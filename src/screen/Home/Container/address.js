import React, { Component } from "react";
import {
  Container,
  Header,
  Button,
  Icon,
  Body,
  H2,
  Content,
  View,
  Text,
  Card,
  CardItem,
  Right,
  Left
} from "native-base";
import { StatusBar, TouchableOpacity, FlatList } from "react-native";
import globalStyles, { colors } from "../../../assets/styles/global";

let address = [
  {
    phone: "0895432043322",
    receiverName: "Heru Julyanto",
    fullAddress: "Jl. H Dasuki askj akjdlja jlkaj lakjsdkj",
    province: "Jawa Barat",
    city: "Bandung",
    zipCode: "40556",
    district: "Cikawet"
  },
  {
    phone: "0895432043322",
    receiverName: "Heru Julyanto",
    fullAddress: "Jl. H Dasuki askj akjdlja jlkaj lakjsdkj",
    province: "Jawa Barat",
    city: "Bandung",
    zipCode: "40556",
    district: "Cikawet"
  }
];

class Address extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderAddress(item) {
    return (
      <Card>
        <CardItem>
          <Left>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              {item.receiverName}
            </Text>
          </Left>
          <Right>
            <Text
              style={{
                color: colors.GREEN_V2,
                fontSize: 22,
                fontWeight: "bold"
              }}
            >
              UTAMA
            </Text>
          </Right>
        </CardItem>
        <CardItem>
          <Text note>{item.phone}</Text>
        </CardItem>
        <CardItem>
          <Text note>{item.fullAddress}</Text>
        </CardItem>
        <CardItem>
          <Text note>
            {item.city} - {item.district}
          </Text>
        </CardItem>
        <CardItem>
          <Text note>
            {item.province} {item.zipCode}
          </Text>
        </CardItem>
      </Card>
    );
  }
  render() {
    return (
      <Container>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Header style={{ backgroundColor: "#FFFFFF", borderBottomWidth: 0 }}>
          <Button
            onPress={() => this.props.navigation.goBack()}
            style={{ width: "15%", paddingLeft: 0 }}
            transparent
          >
            <Icon
              style={{ color: "#74B027" }}
              name="ios-arrow-back"
              type="Ionicons"
            />
          </Button>
          <Body>
            <H2 style={{ fontWeight: "300" }}>ALAMAT SAYA</H2>
          </Body>
        </Header>
        <Content>
          <View style={globalStyles.centerContainer}>
            <View style={{ marginVertical: 12 }}>
              <Button
                onPress={() => this.props.navigation.navigate("AddAddress")}
                style={{
                  width: "55%",
                  backgroundColor: "white",
                  borderColor: colors.GREEN_V2,
                  borderWidth: 1,
                  borderRadius: 5
                }}
              >
                <Icon
                  name="plus"
                  type="Entypo"
                  style={{ color: colors.GREEN_V2 }}
                />
                <Text style={{ color: colors.GREEN_V2 }}>
                  Tambah Alamat Baru
                </Text>
              </Button>
            </View>
            <View style={{ width: "85%" }}>
              <FlatList
                data={address}
                renderItem={({ item }) => this.renderAddress(item)}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

export default Address;
