import { combineReducers } from "redux";

import auth from "./auth";
import signup from "./signup";
import product from "./product";
import users from "./users";
import cart from "./cart";
import checkout from "./checkout";

const app = combineReducers({
  auth,
  signup,
  product,
  users,
  cart,
  checkout
});

export default app;
