import { SET_COURIER } from "../actions/const/checkout";

const initialState = {
  courier: "JNE REG"
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_COURIER:
      return {
        ...state,
        courier: action.payload
      };

    default:
      return state;
  }
};
