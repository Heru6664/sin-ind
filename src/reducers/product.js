import {
  GET_PRODUCT_START,
  GET_PRODUCT_FAILED,
  GET_PRODUCT_SUCCESS,
  GET_DETAIL_PRODUCT
} from "../actions/const/product";

const initialState = {
  products: [],
  detail: {},
  isLoading: false,
  error: null,
  failed: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        failed: false
      };
    case GET_PRODUCT_FAILED:
      return {
        ...state,
        isLoading: false,
        failed: true,
        error: action.payload
      };
    case GET_PRODUCT_SUCCESS:
      return {
        ...state,
        isLoading: false,
        failed: false,
        error: null,
        products: action.payload.data
      };
    case GET_DETAIL_PRODUCT:
      return {
        ...state,
        isLoading: false,
        failed: false,
        detail: action.payload
      };
    default:
      return state;
  }
};
