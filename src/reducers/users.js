import {
  GET_USERS_START,
  GET_USERS_FAILED,
  GET_USERS_SUCCESS,
  GET_USER_START,
  GET_USER_FAILED,
  GET_USER_SUCCESS,
  EDIT_USER_START,
  EDIT_USER_FAILED,
  EDIT_USER_SUCCESS,
  UPLOAD_IMAGE
} from "../actions/const/users";

const initialState = {
  users: [],
  userProfile: {},
  updateUser: {},
  isLoading: false,
  error: null,
  avatar: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS_START:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case GET_USERS_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        users: action.payload
      };
    case GET_USER_START:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case GET_USER_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        userProfile: action.payload
      };
    case EDIT_USER_START:
      return {
        ...state,
        isLoading: true,
        error: null
      };
    case EDIT_USER_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload
      };
    case EDIT_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null
      };
    case UPLOAD_IMAGE:
      return {
        ...state,
        avatar: action.payload
      };
    default:
      return state;
  }
};
