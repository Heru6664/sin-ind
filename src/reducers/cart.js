import {
  INC_TOTAL,
  DEC_TOTAL,
  GET_CART_START,
  GET_CART_FAILED,
  GET_CART_SUCCESS,
  ADD_ITEMS_START,
  ADD_ITEMS_FAILED,
  ADD_ITEMS_SUCCESS,
  GET_LATEST_CART_START,
  GET_LATEST_CART_FAILED,
  GET_LATEST_CART_SUCCESS,
  LIST_ITEMS_CART,
  INC_QUANTITY_START,
  INC_QUANTITY_FAILED,
  INC_QUANTITY_SUCCESS
} from "../actions/const/cart";

const initialState = {
  listItemsCart: [],
  allCarts: [],
  myCart: [],
  error: null,
  isLoading: false,
  failed: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    // case INC_TOTAL:
    //   return {
    //     ...state,
    //     cart: [
    //       ...state.cart.slice(0, action.payload),
    //       {
    //         ...state.cart[action.payload],
    //         total: state.cart[action.payload].total + 1
    //       },
    //       ...state.cart.slice(action.payload + 1)
    //     ]
    //   };
    // case DEC_TOTAL:
    //   return {
    //     ...state,
    //     cart: [
    //       ...state.cart.slice(0, action.payload),
    //       {
    //         ...state.cart[action.payload],
    //         total: state.cart[action.payload].total - 1
    //       },
    //       ...state.cart.slice(action.payload + 1)
    //     ]
    //   };
    case GET_CART_START:
      return {
        ...state,
        error: null,
        isLoading: true,
        failed: false
      };
    case GET_CART_FAILED:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        failed: true
      };
    case GET_CART_SUCCESS:
      return {
        ...state,
        error: null,
        isLoading: false,
        failed: false,
        allCarts: action.payload.data.data
      };
    case ADD_ITEMS_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        failed: false
      };
    case ADD_ITEMS_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        failed: true
      };
    case ADD_ITEMS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        failed: false,
        cart: action.payload
      };
    case GET_LATEST_CART_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        failed: false
      };
    case GET_LATEST_CART_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        failed: true
      };
    case GET_LATEST_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        failed: false,
        myCart: action.payload
      };
    case LIST_ITEMS_CART:
      return {
        ...state,
        listItemsCart: action.payload
      };
    case INC_QUANTITY_START:
      return {
        ...state,
        isLoading: true,
        error: null,
        failed: false
      };
    case INC_QUANTITY_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        failed: true
      };
    case INC_QUANTITY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        failed: false
      };
    default:
      return state;
  }
};
