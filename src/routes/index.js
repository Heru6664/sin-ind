import { createStackNavigator, createAppContainer } from "react-navigation";
import Login from "../screen/Auth/Login";
import Signup from "../screen/Auth/Signup";
import Home from "../screen/Home/Home";
import SplashScreen from "../screen/SplashScreen";
import DetailProduct from "../screen/Track/DetailProduct";
import React from "react";
import { TouchableOpacity } from "react-native";
import { Text, View, Icon, H2 } from "native-base";
import ForgotPassword from "../screen/Auth/ForgotPassword";
import { colors } from "../assets/styles/global";
import NewPassword from "../screen/Auth/ForgotPassword/NewPassword";

const RootStack = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    Signup: {
      screen: Signup
    },
    Home: {
      screen: ({ navigation }) => <Home screenProps={navigation} />,
      navigationOptions: {
        header: null
      }
    },
    SplashScreen: {
      screen: SplashScreen
    },
    DetailProduct: {
      screen: DetailProduct,
      navigationOptions: {
        header: null
      }
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        header: null
      }
    },
    NewPassword: {
      screen: NewPassword,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "SplashScreen"
  }
);

const Root = createAppContainer(RootStack);

export default Root;
