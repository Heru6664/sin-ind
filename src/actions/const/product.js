export const GET_PRODUCT_START = "GET_PRODUCT_START";
export const GET_PRODUCT_FAILED = "GET_PRODUCT_FAILED";
export const GET_PRODUCT_SUCCESS = "GET_PRODUCT_SUCCESS";

export const GET_DETAIL_PRODUCT = "GET_DETAIL_PRODUCT";
