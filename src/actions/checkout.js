import { SET_COURIER } from "./const/checkout";

export const setCourier = data => ({
  type: SET_COURIER,
  payload: data
});
