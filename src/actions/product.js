import {
  GET_PRODUCT_START,
  GET_PRODUCT_FAILED,
  GET_PRODUCT_SUCCESS,
  GET_DETAIL_PRODUCT
} from "./const/product";
import axios from "axios";
import { BASE_URL } from "./api";

const getProductStart = () => ({
  type: GET_PRODUCT_START
});
const getProductFailed = error => ({
  type: GET_PRODUCT_FAILED,
  payload: error
});
const getProductSuccess = data => ({
  type: GET_PRODUCT_SUCCESS,
  payload: data
});

export const getProducts = user => dispatch => {
  return new Promise((resolve, reject) => {
    dispatch(getProductStart());
    return axios({
      method: "get",
      url: `${BASE_URL}products`,
      headers: {
        Uid: user.uid,
        Client: user.client,
        ["Access-Token"]: user.token
      }
    })
      .then(res => {
        dispatch(getProductSuccess(res.data));
        return resolve(res.data.data);
      })
      .catch(error => {
        dispatch(getProductFailed(error));
        reject(error.response.data.errors);
        return ToastAndroid.showWithGravityAndOffset(
          error.response.data.errors[0],
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      });
  });
};

export const getDetail = data => ({
  type: GET_DETAIL_PRODUCT,
  payload: data
});
