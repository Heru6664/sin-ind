import {
  GET_CART_START,
  GET_CART_FAILED,
  GET_CART_SUCCESS,
  ADD_ITEMS_START,
  ADD_ITEMS_FAILED,
  ADD_ITEMS_SUCCESS,
  GET_LATEST_CART_START,
  GET_LATEST_CART_FAILED,
  GET_LATEST_CART_SUCCESS,
  LIST_ITEMS_CART,
  INC_QUANTITY_START,
  INC_QUANTITY_FAILED,
  INC_QUANTITY_SUCCESS,
  DEC_QUANTITY_START,
  DEC_QUANTITY_FAILED,
  DEC_QUANTITY_SUCCESS
} from "./const/cart";
import { store } from "../store";
import Axios from "axios";
import _ from "lodash";
import { BASE_URL } from "./api";

const getCartStart = () => ({
  type: GET_CART_START
});
const getCartFailed = error => ({
  type: GET_CART_FAILED,
  payload: error
});
const getCartSuccess = data => ({
  type: GET_CART_SUCCESS,
  payload: data
});

export const getAllCart = item => dispatch => {
  dispatch(getCartStart());
  console.log("user_id:", item.user_id);
  return Axios.get(`${BASE_URL}carts`, {
    headers: {
      Uid: item.uid,
      Client: item.client,
      ["Access-Token"]: item.token
    }
  })
    .then(res => {
      dispatch(getCartSuccess(res));
      console.log("res", res);
      return res;
    })
    .catch(error => {
      dispatch(getCartFailed(error.response));
      console.log("error", error);
    });
};

const addItemStart = () => ({
  type: ADD_ITEMS_START
});
const addItemFailed = error => ({
  type: ADD_ITEMS_FAILED,
  payload: error
});
const addItemSuccess = data => ({
  type: ADD_ITEMS_SUCCESS,
  payload: data
});

export const addItemsToCart = item => dispatch => {
  // const csrfToken = _.find(document.getElementsByTagName("meta"), meta => {
  //   return meta.name === "csrf-token";
  // }).content;

  // console.log("csrf", csrfToken);

  dispatch(addItemStart());
  return Axios({
    method: "post",
    url: `${BASE_URL}carts/${item.cart_id}/items`,
    data: {
      cart_item: {
        product_id: parseInt(item.product_id),
        quantity: parseInt(item.quantity)
      }
    },
    headers: {
      Uid: item.uid,
      Client: item.client,
      ["Access-Token"]: item.token
      // ["X-CSRF-TOKEN"]: csrfToken
    }
  })
    .then(res => {
      dispatch(addItemSuccess(res.data.data));
      return res;
    })
    .catch(err => {
      dispatch(addItemFailed(err));
    });
};

const getLatestCartStart = () => ({
  type: GET_LATEST_CART_START
});
const getLatestCartFailed = error => ({
  type: GET_LATEST_CART_FAILED,
  payload: error
});
const getLatestSuccess = cart => ({
  type: GET_LATEST_CART_SUCCESS,
  payload: cart
});

export const getLatestCart = ({ cart_id, options }) => dispatch => {
  console.log("cart_id", cart_id);
  console.log("optionss", options);

  dispatch(getLatestCartStart());
  return Axios.get(`${BASE_URL}carts/${cart_id}/items`, {
    headers: {
      Uid: options.uid,
      Client: options.client,
      ["Access-Token"]: options.token
    }
  })
    .then(res => {
      dispatch(getLatestSuccess(res));
      return res;
    })
    .catch(err => {
      dispatch(getLatestCartFailed(err));
    });
};

export const listItemsCart = items => ({
  type: LIST_ITEMS_CART,
  payload: items
});

const incQtyStart = () => ({
  type: INC_QUANTITY_START
});
const incQtyFailed = error => ({
  type: INC_QUANTITY_FAILED,
  payload: error
});
const incQtySuccess = qty => ({
  type: INC_QUANTITY_SUCCESS,
  payload: qty
});
export const incQuantity = item => dispatch => {
  console.log("datassas", item);

  dispatch(incQtyStart());
  return Axios.put(
    `${BASE_URL}carts/${item.cart_id}/items/${item.id}`,
    {
      "cart_item": {
        "product_id": parseInt(item.product_id),
        "quantity": parseInt(item.quantity)
      }
    },
    {
      headers: {
        Uid: item.uid,
        Client: item.client,
        ["Access-Token"]: item.token
      }
    }
  )
    .then(res => {
      dispatch(incQtySuccess(res));
    })
    .catch(err => {
      dispatch(incQtyFailed(err));
    });
};

const decQtyStart = () => ({
  type: DEC_QUANTITY_START
});
const decQtyFailed = error => ({
  type: DEC_QUANTITY_FAILED,
  payload: error
});
const decQtySuccess = qty => ({
  type: DEC_QUANTITY_SUCCESS,
  payload: qty
});
export const decQuantity = item => dispatch => {
  dispatch(decQtyStart());
  return Axios.put(
    `${BASE_URL}carts/${item.cart_id}/items/${item.id}`,
    {
      cart_item: {
        product_id: parseInt(item.product_id),
        quantity: parseInt(item.quantity)
      }
    },
    {
      headers: {
        Uid: item.uid,
        Client: item.client,
        ["Access-Token"]: item.token
      }
    }
  )
    .then(res => {
      dispatch(decQtySuccess(res));
    })
    .catch(err => {
      dispatch(decQtyFailed(err));
    });
};
