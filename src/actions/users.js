import {
  GET_USERS_START,
  GET_USERS_FAILED,
  GET_USERS_SUCCESS,
  GET_USER_START,
  GET_USER_FAILED,
  GET_USER_SUCCESS,
  EDIT_USER_START,
  EDIT_USER_FAILED,
  EDIT_USER_SUCCESS,
  UPLOAD_IMAGE
} from "./const/users";
import axios from "axios";
import { ToastAndroid } from "react-native";
import { BASE_URL } from "./api";

const getUsersStart = () => ({
  type: GET_USERS_START
});
const getUsersFailed = error => ({
  type: GET_USERS_FAILED,
  payload: error
});
const getUsersSuccess = users => ({
  type: GET_USERS_SUCCESS,
  payload: users
});

export const getUsers = () => dispatch => {
  return new Promise((resolve, reject) => {
    dispatch(getUsersStart());
    return axios
      .get(`${BASE_URL}users`)
      .then(res => {
        dispatch(getUsersSuccess(res));
        return resolve(true);
      })
      .catch(error => {
        dispatch(getUsersFailed(error));
        reject(error.response.data.errors);
        return ToastAndroid.showWithGravityAndOffset(
          error.response.data.errors[0],
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      });
  });
};

const getUserStart = () => ({
  type: GET_USER_START
});
const getUserFailed = error => ({
  type: GET_USER_FAILED,
  payload: error
});
const getUserSuccess = userData => ({
  type: GET_USER_SUCCESS,
  payload: userData
});

export const getUser = id => dispatch => {
  return new Promise((resolve, reject) => {
    dispatch(getUserStart());
    return axios
      .get(`${BASE_URL}users/${id}`)
      .then(res => {
        dispatch(getUserSuccess(res));
        return resolve(true);
      })
      .catch(error => {
        dispatch(getUserFailed(error));
        reject(error.response.data.errors);
        return ToastAndroid.showWithGravityAndOffset(
          error.response.data.errors[0],
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      });
  });
};

const editUserStart = () => ({
  type: EDIT_USER_START
});
const editUserFailed = error => ({
  type: EDIT_USER_FAILED,
  payload: error
});
const editUserSuccess = data => ({
  type: EDIT_USER_SUCCESS,
  payload: data
});

export const editUser = user => dispatch => {
  return new Promise((resolve, reject) => {
    console.log("usersss:", user);
    dispatch(editUserStart());
    return axios({
      method: "put",
      url: `${BASE_URL}profile/${user.id}`,
      data: {
        username: user.userName,
        first_name: user.firstName,
        last_name: user.lastName,
        telephone: user.telephone,
        email: user.email,
        avatar: user.img
      },
      headers: {
        Uid: user.uid,
        Client: user.client,
        ["Access-Token"]: user.token
      }
    })
      .then(res => {
        dispatch(editUserSuccess(res));
        return resolve(true);
      })
      .catch(error => {
        dispatch(editUserFailed(error));
        reject(error.response.data.errors);
        return ToastAndroid.showWithGravityAndOffset(
          error.response.data.errors[0],
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          25,
          50
        );
      });
  });
};

export const uploadProfile = obj => ({
  type: UPLOAD_IMAGE,
  payload: obj
});
