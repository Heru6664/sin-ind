import { StyleSheet, Dimensions } from "react-native";

const screenHeight = Dimensions.get("window").height;
const screenWidth = Dimensions.get("window").width;

export const height = screenHeight < screenWidth ? screenWidth : screenHeight;
export const width = screenWidth < screenHeight ? screenWidth : screenHeight;

export const colors = {
  PRIMARY: "#39712c",
  GREEN_V2: "#72b212",
  WHITE: "#fff"
};

export const Metrics = {
  screenWidth: Dimensions.get("screen").width,
  screenHeight: Dimensions.get("screen").height
};

const globalStyles = StyleSheet.create({
  errorWrapper: {
    textAlign: "right",
    marginTop: 10,
    marginBottom: 10,
    color: "#f52740",
    fontSize: 11
  },
  centerContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  contentContainer: {
    marginHorizontal: "5%",
    marginVertical: height * 0.015,
    paddingRight: height * 0.15,
    flexDirection: "row",
    width: "100%",
    height: width * 0.39
  },
  cardContainer: {
    width: width * 0.45,
    height: width * 0.35,
    borderRadius: 25,
    paddingTop: 10,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: width * 0.015,
    marginVertical: height * 0.05,
    elevation: 7
  },
  alignCenterContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: colors.white
  },
  spaceBetweenContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  splashScreenLogo: {
    height: width * 0.4,
    width: width * 0.4,
    marginBottom: width * 0.08
  }
});

export default globalStyles;
