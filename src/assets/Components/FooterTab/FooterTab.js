import React, { Component } from "react";
import { createBottomTabNavigator, createAppContainer } from "react-navigation";
import { Platform } from "react-native";
import { Button, Text, Icon, Footer, FooterTab } from "native-base";
import {
  notification,
  product,
  profile,
  Home
} from "../../../screen/Home/Container";
import { colors } from "../../styles/global";
import showAll from "../../../screen/Home/Container/showAll";
import Wishlist from "../../../screen/Home/Container/wishlist";
import EditProfile from "../../../screen/Home/Container/editProfile";
import Address from "../../../screen/Home/Container/address";
import AddAddress from "../../../screen/Home/Container/addAddress";
import ChangePassword from "../../../screen/Home/Container/changePassword";
import Cart from "../../../screen/Track/Cart";
import Checkout from "../../../screen/Track/Checkout";
import Courier from "../../../screen/Track/Courier";

const MainScreenNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Home
    },
    Product: {
      screen: product
    },
    Notification: {
      screen: notification
    },
    Profile: {
      screen: profile,
      navigationOptions: {
        header: null
      }
    },
    ShowAll: {
      screen: showAll
    },
    Wishlist: {
      screen: Wishlist
    },
    EditProfile: {
      screen: EditProfile
    },
    Address: {
      screen: Address
    },
    AddAddress: {
      screen: AddAddress
    },
    ChangePassword: {
      screen: ChangePassword
    },
    Cart: {
      screen: Cart
    },
    Checkout: {
      screen: Checkout
    },
    Courier: {
      screen: Courier
    }
  },
  {
    tabBarPosition: "bottom",
    tabBarComponent: props => {
      return (
        <Footer style={{ backgroundColor: "white", marginBottom: -2 }}>
          <FooterTab style={{ backgroundColor: "white" }}>
            <Button
              vertical
              style={{
                backgroundColor: "white"
              }}
              active={props.navigation.state.index === 0}
              onPress={() => props.navigation.navigate("Home")}
            >
              <Icon
                type="MaterialCommunityIcons"
                name="home-outline"
                style={{
                  color:
                    props.navigation.state.index === 0 ? "#74B027" : "#787878"
                }}
              />
              <Text
                style={{
                  color:
                    props.navigation.state.index === 0 ? "#74B027" : "#787878"
                }}
              >
                Home
              </Text>
            </Button>
            <Button
              vertical
              style={{
                backgroundColor: "white"
              }}
              active={props.navigation.state.index === 1}
              onPress={() => props.navigation.navigate("Product")}
            >
              <Icon
                style={{
                  color:
                    props.navigation.state.index === 1 ? "#74B027" : "#787878"
                }}
                name="list-ul"
                type="FontAwesome"
              />
              <Text
                style={{
                  color:
                    props.navigation.state.index === 1 ? "#74B027" : "#787878"
                }}
              >
                Product
              </Text>
            </Button>
            <Button
              vertical
              style={{
                backgroundColor: "white"
              }}
              active={props.navigation.state.index === 2}
              onPress={() => props.navigation.navigate("Notification")}
            >
              <Icon
                style={{
                  color:
                    props.navigation.state.index === 2 ? "#74B027" : "#787878"
                }}
                name="notifications-none"
                type="MaterialIcons"
              />
              <Text
                style={{
                  color:
                    props.navigation.state.index === 2 ? "#74B027" : "#787878"
                }}
              >
                Notification
              </Text>
            </Button>
            <Button
              vertical
              style={{
                backgroundColor: "white"
              }}
              active={props.navigation.state.index === 3}
              onPress={() => props.navigation.navigate("Profile")}
            >
              <Icon
                style={{
                  color:
                    props.navigation.state.index === 3 ? "#74B027" : "#787878"
                }}
                name="account-outline"
                type="MaterialCommunityIcons"
              />
              <Text
                style={{
                  color:
                    props.navigation.state.index === 3 ? "#74B027" : "#787878"
                }}
              >
                Profile
              </Text>
            </Button>
          </FooterTab>
        </Footer>
      );
    },
    tabBarOptions: {
      activeTintColor: colors.WHITE,
      activeBackgroundColor: colors.DARK_BLUE,
      iconStyle: { flex: 1, width: 15, height: 15, padding: 0 },
      // labelStyle: { fontSize: 10, paddingHorizontal: 0 },
      style: {
        backgroundColor:
          Platform.OS === "ios" ? colors.DARK_BLUE : colors.DARK_BLUE,
        marginBottom: Platform.OS == "android" ? -10 : 0,
        // height: Metrics.screenHeight * 0.1,
        borderTopColor: "#fafafa"
        // flex: 1,
      },
      indicatorStyle: {
        flex: 1,
        backgroundColor: colors.DARK_BLUE
      },
      tabStyle: {
        flex: 1,
        backgroundColor: Platform.OS == "android" ? colors.DARK_BLUE : null
      },
      labelStyle: {
        fontSize: 10
      }
    }
  }
);

const Tab = createAppContainer(MainScreenNavigator);

export default Tab;
